<?php
if ( !function_exists('auth_redirect') ){
  function auth_redirect($redirect=""){
    $CI =& get_instance();
      $member_login = $CI->session->userdata('member_login');
    if($redirect!="") $CI->session->set_userdata('redirect', $redirect);
    if(empty($member_login)) redirect('login');
    else return true;
  }
}

if ( !function_exists('auth_redirect_admin') ){
  function auth_redirect_admin(){
    $CI =& get_instance();
    if(empty($CI->session->userdata('admin_login'))) redirect(base_url('login'));
    else return true;
  }
}

if ( !function_exists('is_admin') ){
  function is_admin(){
    $CI =& get_instance();
    $member_login = $CI->session->userdata('member_login');
    if(!empty($member_login)){
      $where = array(
        'email'=> $CI->session->userdata('member_login')
      );
      $data_member = $CI->member_model->read_member($where);
      if($data_member->num_rows()!=0){
        if($data_member->row()->type=='admin')
        return 1;
        else return 0;
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_current_member') ){
  function get_current_member(){
    $CI =& get_instance();
    if(!empty($CI->session->userdata('member_login'))){
      $where = array(
        'username'=> $CI->session->userdata('member_login')
      );
      $data_member = $CI->member_model->read_member($where);
      if($data_member->num_rows()!=0){
        return $data_member->row();
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_all_plants_list') ){
  function get_all_plants_list(){
    $CI =& get_instance();
      $data = $CI->plants_model->read_plants();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_project_team') ) {
    function get_all_project_team ($member_id) {
        $CI=& get_instance();
        $sql = "SELECT B.name member_name, B.email member_email, C.name plant_name FROM `device_owner` AS A
JOIN member AS B ON B.id=A.member_id
JOIN plant AS C ON C.member_id=A.member_id
WHERE C.member_id=".$member_id;
            $data = $CI->device_owner_model->custom_sql($sql);
        if($data->num_rows()!=0) return $data;
        else return false;
    } 
}
if ( !function_exists('get_all_article')){
    function get_all_article () {
        $CI=& get_instance();
        $data = $CI->article_model->read_article_model();
        if($data->num_rows()!=0) {
            return $data;
        } else return false;
    }
}

if ( !function_exists('get_project_list') ){
  function get_project_list($limit="", $member_id){
    $CI =& get_instance();
      $sql = "SELECT A.*, B.name plants_name, B.picture plants_picture, B.latin_name plants_latin_name, B.harvest_time harvest_time, AVG(D.suhu) avg_temp, AVG(D.kelembaban) avg_hum, AVG(D.moisture) avg_mois, AVG(D.cahaya) avg_light, AVG(D.ph) avg_ph
      FROM plant as A
      JOIN plants as B ON B.id = A.plants_id 
      JOIN plant_device as C ON C.plant_id = A.id
      JOIN sensor_data as D ON D.device_id = C.device_id
      WHERE A.member_id='".$member_id."' 
      ";
      if($limit!=""){
          $sql.="LIMIT 0,".$limit;
      }
      $data = $CI->plant_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_avg_moisture') ){
  function get_avg_sensor($plant_id){
    $CI =& get_instance();
      $sql = "SELECT AVG(D.suhu) avg_temp, AVG(D.kelembaban) avg_hum, AVG(D.moisture) avg_mois, AVG(D.cahaya) avg_light, AVG(D.ph) avg_ph
      FROM plant as A
      JOIN plant_device as C ON C.plant_id = A.id
      JOIN sensor_data as D ON D.device_id = C.device_id
      WHERE A.id='".$plant_id."' 
      ";
      $data = $CI->plant_model->custom_sql($sql);
      if($data->num_rows()!=0){
          $data_row = $data->row();
//          switch ($type_sensor) {
//            case 'temperature':
//                return $data_row->avg_temp;
//                break;
//            case 'light':
//                return $data_row->avg_light;
//                break;
//            case 'ph':
//                return $data_row->avg_ph;
//                break;
//            case 'humidity':
//                return $data_row->avg_hum;
//                break;
//            case 'moisture':
//                return $data_row->avg_mois;
//                break;
//        }
          return $data_row;
      } else return false;
  }
}

if ( !function_exists('get_total_devices') ){
  function get_total_devices($member_id){
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT IFNULL(count(plant_device.id), 0) total_device FROM plant 
      JOIN plant_device ON plant_device.plant_id = plant.id WHERE plant.member_id='".$member_id."'";
      $data = $CI->plant_model->custom_sql($sql);
      $count+= $data->row()->total_device;
      $sql = "SELECT * FROM device_owner where member_id='".$member_id."'";
      $data = $CI->plant_model->custom_sql($sql);
      $count+=$data->num_rows();
      return $count;
  }
}

if ( !function_exists('get_total_plants') ){
  function get_total_plants($member_id){
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT IFNULL(count(id), 0) total_plant FROM plant WHERE member_id='".$member_id."'";
      $data = $CI->plant_model->custom_sql($sql);
      $count+= $data->row()->total_plant;
      return $count;
  }
}

if ( !function_exists('get_total_farming_days') ){
  function get_total_farming_days($member_id, $by=""){
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT DATE(datecreated) date FROM plant ";
      if($by=="member_id") $sql.="WHERE member_id='".$member_id."'";
      if($by=="plant_id") $sql.="WHERE id='".$member_id."'";
      $sql.="ORDER BY datecreated ASC LIMIT 0,1";
      $data = $CI->plant_model->custom_sql($sql);
      $date_created = date('Y-m-d');
      if($data->num_rows()!=0) $date_created = $data->row()->date;
      $date_now = date('Y-m-d');
      return date_range($date_created, $date_now);
  }
}

if ( !function_exists('get_devices_list') ){
  function get_devices_list($member_id){ 
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT plant.*, plant_device.device_id device_id, device.device_code device_code, DATE(device.datecreated) date_created 
      FROM plant 
      JOIN plant_device ON plant_device.plant_id = plant.id 
      JOIN device ON device.id = plant_device.device_id
      WHERE plant.member_id='".$member_id."'
      GROUP BY plant_device.device_id
      ";
      $data = $CI->plant_model->custom_sql($sql);
      if($data->num_rows()!=0) return $data;
      else return false;
  }
}

if ( !function_exists('get_device_status') ){
  function get_device_status($device_id){ 
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT datecreated latest_update FROM sensor_data 
      WHERE device_id='".$device_id."'
      ORDER BY datecreated DESC
      LIMIT 0,1
      ";
      $data = $CI->plant_model->custom_sql($sql);
      if($data->num_rows()!=0) {
          $latest_update = new DateTime($data->row()->latest_update);
          $now = date('Y-m-d H:i:s');
          $diff = $latest_update->diff(new DateTime($now));
          $last_min = $diff->i;
          if($last_min<3) return 'Online';
          return 'Offline';
      }
      else return 'Offline';
  }
}



if ( !function_exists('date_range') ){
  function date_range($date_start, $date_end){
    $date1=date_create($date_start);
    $date2=date_create($date_end);
    $diff=date_diff($date1,$date2);
    return (INT)$diff->format("%a");
  }
}

if ( !function_exists('get_total_water_usage') ){
  function get_total_water_usage($member_id, $by=""){
    $CI =& get_instance();
      $count = 0;
      $sql = "SELECT IFNULL(sum(water_usage.debet), 0) total_debet FROM plant 
      JOIN plant_device ON plant_device.plant_id = plant.id 
      JOIN water_usage ON water_usage.device_id = plant_device.device_id ";
      if($by=="member_id")
        $sql.="WHERE plant.member_id='".$member_id."'";
      if($by=="plant_id")
          $sql.="WHERE plant.id='".$member_id."'";
      $data = $CI->plant_model->custom_sql($sql);
      $count+= $data->row()->total_debet;
      return (INT)$count;
  }
}


if ( !function_exists('get_total_expenses') ){
  function get_total_expenses(){
    $CI =& get_instance();
      $sql ="SELECT IFNULL(SUM(nominal),0) AS total FROM expenses";
      $data = $CI->expenses_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->total;
      } else return false;
  }
}

if ( !function_exists('get_total_tax') ){
  function get_total_tax(){
    $CI =& get_instance();
      $sql ="SELECT IFNULL(SUM(nominal),0) AS total FROM expenses WHERE category='tax'";
      $data = $CI->expenses_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->total;
      } else return false;
  }
}

if ( !function_exists('get_total_income') ){
  function get_total_income(){
    $CI =& get_instance();
      $sql ="SELECT IFNULL(SUM(nominal),0) AS total FROM income";
      $data = $CI->expenses_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->total;
      } else return false;
  }
}

if ( !function_exists('get_total_inventaris') ){
  function get_total_inventaris(){
    $CI =& get_instance();
      $sql ="SELECT IFNULL(SUM(nominal),0) AS total FROM expenses WHERE category='assets'";
      $data = $CI->inventory_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->total;
      } else return false;
  }
}

if ( !function_exists('get_all_pembelian_produk_list') ){
  function get_all_pembelian_produk_list(){
    $CI =& get_instance();
      $data = $CI->pembelian_produk_model->read_pembelian_produk();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_produk_list') ){
  function get_all_produk_list(){
    $CI =& get_instance();
      $data = $CI->produk_model->read_produk();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_nota_list') ){
  function get_all_nota_list(){
    $CI =& get_instance();
      $data = $CI->nota_model->read_nota();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_sj_list') ){
  function get_all_sj_list(){
    $CI =& get_instance();
      $data = $CI->surat_jalan_model->read_surat_jalan();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_redeem_list') ){
  function get_all_redeem_list(){
    $CI =& get_instance();
    $where = array('type'=>'out');
      $data = $CI->point_model->read_point($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pekerja_list') ){
  function get_all_pekerja_list(){
    $CI =& get_instance();
      $data = $CI->pekerja_model->read_pekerja();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_penjualan_list') ){
  function get_all_penjualan_list(){
    $CI =& get_instance();
      $data = $CI->penjualan_model->read_penjualan();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pembelian_list') ){
  function get_all_pembelian_list(){
    $CI =& get_instance();
      $data = $CI->pembelian_model->read_pembelian();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pelanggan_list') ){
  function get_all_pelanggan_list(){
    $CI =& get_instance();
      $data = $CI->pelanggan_model->read_pelanggan();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}


if ( !function_exists('get_all_member_list') ){
  function get_all_member_list($where){
    $CI =& get_instance();
      $data_member = $CI->member_model->read_member($where);
      if($data_member->num_rows()!=0){
        return $data_member;
      } else return false;
  }
}

if ( !function_exists('get_all_nota_list') ){
  function get_all_nota_list(){
    $CI =& get_instance();
      $data = $CI->nota_model->read_nota();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_all_pekerja_list') ){
  function get_all_pekerja_list($site_code){
    $CI =& get_instance();
    $data_customer = $CI->pekerja_model->read_pekerja($where);
    if($data_customer->num_rows()!=0){
        return $data_customer;
      } else return false;
  }
}

if ( !function_exists('get_all_produksi') ){
  function get_all_produksi(){
    $CI =& get_instance();
      $data_produksi = $CI->produksi_model->read_produksi();
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('money') ){
  function money($number){
    return "Rp.".number_format($number,0,",",".");
  }
}

if ( !function_exists('generate_unique') ){
  function generate_unique(){
    return rand(1,9).rand(1,9).rand(1,9);
  }
}

if( !function_exists('hari')){
  function hari($hari){
    $harinya = "";
    switch ($hari) {
        case 'Sun':
        $harinya = "Minggu";
        break;
        case 'Mon':
        $harinya = "Senin";
        break;
        case 'Tue':
        $harinya = "Selasa";
        break;
        case 'Wed':
        $harinya = "Rabu";
        break;
        case 'Thu':
        $harinya = "Kamis";
        break;
        case 'Fri':
        $harinya = "Jumat";
        break;
        case 'Sat':
        $harinya = "Sabtu";
        break;
    }
    return $harinya;
  }
}

if( !function_exists('bulan')){
  function bulan($bulan){
    $bulannya = "";
    switch ($bulan) {
        case 'Jan':
        $bulannya = "Januari";
        break;
        case 'Feb':
        $bulannya = "Februari";
        break;
        case 'Mar':
        $bulannya = "Maret";
        break;
        case 'Apr':
        $bulannya = "April";
        break;
        case 'May':
        $bulannya = "Mei";
        break;
        case 'Jun':
        $bulannya = "Juni";
        break;
        case 'Jul':
        $bulannya = "Juli";
        break;
        case 'Aug':
        $bulannya = "Agustus";
        break;
        case 'Sep':
        $bulannya = "September";
        break;
        case 'Oct':
        $bulannya = "Oktober";
        break;
        case 'Nov':
        $bulannya = "November";
        break;
        case 'Dec':
        $bulannya = "Desember";
        break;
    }
    return $bulannya;
  }
}
if ( !function_exists('get_firstname') ){
  function get_firstname($name){
      $name_explode  = explode(' ', $name);
    return $name_explode[0];
  }
}

if ( !function_exists('clean_content') ){
  function clean_content($content){
      return strip_tags($content, '<a><b><br><div><em><i><li><table><td><tr><span><sub><sup><strong><u><ul>');
  }
}

if ( !function_exists('view_short') ){
  function view_short($title){
      return substr($title,0,50);
  }
}

if ( !function_exists('phone_number') ){
  function phone_number($number){
      $first = substr($number, 0,1);
      if($first=="0") return "62".substr($number, 1);
      if($first=="+") return substr($number, 1);
      else return $number;
  }
}
if ( !function_exists('get_statistic') ){
  function get_statistic(){
    $CI =& get_instance();
      $sql = "SELECT SUM(penjualan_detail.qty) jumlah, pembelian_produk.nama_produk nama_produk FROM penjualan
      JOIN penjualan_detail ON penjualan_detail.id_penjualan = penjualan.id_penjualan
      JOIN produk ON produk.id_produk=penjualan_detail.id_produk
      JOIN pembelian ON pembelian.id_pembelian = produk.id_pembelian
      JOIN pembelian_produk ON pembelian_produk.id_pembelian_produk=pembelian.id_pembelian_produk
      GROUP BY penjualan_detail.id_produk desc
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_statistic_daily') ){
  function get_statistic_daily(){
    $CI =& get_instance();
      $sql = "SELECT SUM(total_keseluruhan) jumlah, date(datecreated) date FROM penjualan
      GROUP BY DATE(datecreated)
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_profit_by_date') ){
  function get_profit_by_date($date){
    $CI =& get_instance();
      $sql = "SELECT IFNULL(SUM(total_keseluruhan),0) jumlah, date(datecreated) date FROM penjualan
      WHERE DATE(datecreated)= '".$date."'
      ";
      $data = $CI->penjualan_detail_model->custom_sql($sql);
      if($data->num_rows()!=0){
        return $data->row()->jumlah;
      } else return 0;
  }
}

?>
