<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    function post_collection(){
        $data = json_decode($this->input->get('data'), true);
        //checking json
        if(!empty($data)){
        $temperature = $data['temperature'];
        $humidity = $data['humidity'];
        $moisture =$data['moisture'];
        $lux = $data['lux'];
        $ph = $data['ph'];
        $device_code = $data['device_code'];
        $where       = array('device_code'=>$device_code->id);
        $device_query = $this->device_model->read_device($where);
        if($device_query->num_rows()!=0){
          $device_data_query = $device_query->row();
          $device_id = $device_data_query->id;
          $where     = array('device_id'=>$device_id);
          $plants_query   = $this->plant_device_model->read_plants_device($where);
          if($plants_query->num_rows()!=0){
            $plants_data_query = $plants_query->rowa();
            $plants_id = $plants_data_query->plant_id;
          }
        }
        
        //checking input
//        if($temperature!="" && $humidity!="" && $moisture!="" && $lux!="" && $device_code!="" && $ph!=""){             $plants_id   = $this->plants_model->read_plants($where);
   $plants_id   = $this->plants_model->read_plants($where);

            //checking mac
            $where = array('device_code'=>$device_code);
            $code_check = $this->device_model->read_device($where);
            if($code_check->num_rows()!=0){
                $device_data = $code_check->row();
                //input value;
                $data_in = array('device_id'=>$device_data->id, 'suhu'=>$temperature, 'kelembaban'=>$humidity, 'moisture'=>$moisture, 'cahaya'=>$lux, 'ph'=>$ph, 'datecreated'=>date('Y-m-d H:i:s'),'datemodified'=>date('Y-m-d H:i:s'));
                $this->sensor_data_model->create_sensor_data($data_in);
                $data_json = array(
                'status'=>'200',
                'response'=>'Data Sent'
            );
            die(json_encode($data_json));
            } else {
                $data_json = array(
                'status'=>'400',
                'response'=>'Device ID isn\'t recognize'
            );
            die(json_encode($data_json));
            }
//        }  else {
//            $data_json = array(
//                'status'=>'400',
//                'response'=>'Bad Request'
//            );
//            die(json_encode($data_json));
//        }
        } else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
    }
    
    function post_water_debet(){
        $data = json_decode($this->input->get('data'), true);
        //checking json
        if(!empty($data)){
        $debet = $data['debet'];
        $device_code = $data['device_code'];
        //checking input
        if($device_code!=""){
            //checking mac
            $where = array('device_code'=>$device_code);
            $code_check = $this->device_model->read_device($where);
            if($code_check->num_rows()!=0){
                $device_data = $code_check->row();
                //input temperature;
                $data_in = array('device_id'=>$device_data->id, 'debet'=>$debet, 'datecreated'=>date('Y-m-d H:i:s'),'datemodified'=>date('Y-m-d H:i:s'));
                $this->water_usage_model->create_water_usage($data_in);
                $data_json = array(
                'status'=>'200',
                'response'=>'Data Sent'
            );
            die(json_encode($data_json));
            } else {
                $data_json = array(
                'status'=>'400',
                'response'=>'Device ID isn\'t recognize'
            );
            die(json_encode($data_json));
            }
        }  else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
        } else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
    }
    
    function register_device(){
        $data = json_decode($this->input->get('data'), true);
        //checking json
        if(!empty($data)){
            $mac = $data['mac']; 
            $otp = $data['otp'];
            $username = $data['username'];
            //cek otp
            $where = array('otp'=>$otp);
            $otp_qry  = $this->otp_model->read_otp($where);
            if($otp_qry->num_rows()!=0){
                $otp_data = $otp_qry->row();
                $device_code = $otp_data->device_code;
                //cek member
                $where = array('username'=>$username);
                $member = $this->member_model->read_member($where);
                if($member->num_rows()!=0){
                    $member_data = $member->row(); 
                    //cek availability
                    $where = array('device_code'=>$device_code, 'mac'=>$mac);
                    $flag = $this->device_model->read_device($where); 
                    if($flag->num_rows()==0){
                         $data = array(
                             'device_code'=>$device_code, 
                             'mac'=>$mac, 
                             'member_id'=>$member_data->id, 
                             'current_action'=>'stop_sprinkling',
                             'datecreated'=>date('Y-m-d H:i:s'),
                             'datemodified'=>date('Y-m-d H:i:s')
                                      );
                         $flag_device = $this->device_model->create_device($data);
                         $data_json = array(
                        'status'=>'200',
                        'response'=>'Data Sent',
                        'data'=>$device_code
                    );
                    die(json_encode($data_json)); 
                    } else {
                        $data_json = array(
                'status'=>'400',
                'response'=>'Device is already registered'
            );
            die(json_encode($data_json));
                    }
                } else { 
                    $data_json = array(
                'status'=>'400',
                'response'=>'Invalid Username'
            );
            die(json_encode($data_json));
                }

            } else {
                $data_json = array(
                'status'=>'400',
                'response'=>'Invalid OTP'
            );
            die(json_encode($data_json));
            }
        } else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
    }
    
    function get_otp(){
        $device_code = $this->input->get('device_code');
        if($device_code!=""){
            $otp = hexdec(substr(uniqid(rand(0,100)),0,5));
            $data= array('device_code'=>$device_code, 'otp'=>$otp, 'datecreated'=>date('Y-m-d H:i:s'), 'datemodified'=>date('Y-m-d H:i:s'));
            $this->otp_model->create_otp($data);
            $data_json = array(
                'status'=>'200',
                'data'=>$otp,
                'response'=>'Data Sent'
            );
            die(json_encode($data_json));
        } else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
        
    }
    
    function check_device_code(){
        $data = json_decode($this->input->get('data'), true);
        //checking json
        if(!empty($data)){
            $mac = $data['mac'];
            //cek availability
            $where = array('mac'=>$mac);
            $flag = $this->device_model->read_device($where); 
            if($flag->num_rows()!=0){
                $data_json = array(
                'status'=>'200',
                'data'=>$flag->row()->device_code,
                'response'=>'Data Sent'
            );
            die(json_encode($data_json));
            } else {
                $data_json = array(
                'status'=>'400',
                'response'=>'Invalid MAC'
            );
            die(json_encode($data_json));
            }
        } else {
            $data_json = array(
                'status'=>'400',
                'response'=>'Bad Request'
            );
            die(json_encode($data_json));
        }
    }
    
}
?>