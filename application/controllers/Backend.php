<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    public function index()
    {
        auth_redirect();
        $notifikasi      = array();
        $member          = get_current_member();
        $data['member']  = $member;
        $data['title']   = COMPANY_NAME.' | Dashboard';
        $data['content'] = VIEW_BACK . 'dashboard';
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function add_plant()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Add Plant';
        $data['content'] = VIEW_BACK . 'add_plant';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function devices()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Devices';
        $data['content'] = VIEW_BACK . 'devices';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function add_device()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Device';
        $data['content'] = VIEW_BACK . 'add_device';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function projects()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Projects';
        $data['content'] = VIEW_BACK . 'projects';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
        public function select_project()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Select Project';
        $data['content'] = VIEW_BACK . 'select_project';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
            public function detail_report()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Detail Report';
        $data['content'] = VIEW_BACK . 'detail_report';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
            public function tips_trick()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Tips & Tricks';
        $data['content'] = VIEW_BACK . 'tips_trick';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }

     public function start_planting()
    {
        auth_redirect();
        $mode               = $this->input->get('mode');
        $plants_id          = $this->input->get('plants_id');
        if($mode!="" && $plants_id!=""){
        $data['title']      = COMPANY_NAME.' | Start Planting';
        if($mode=='sri_planting')
        $data['content']    = VIEW_BACK . 'sri_planting';
        else 
        $data['content']    = VIEW_BACK . 'pro_planting';
        $where              = array('id'=>$plants_id);
        $plants             = $this->plants_model->read_plants($where);
        if($plants->num_rows()!=0){
        $member             = get_current_member();
        $data['member']     = $member;
        $data['plants']     = $plants->row();
        $data['plants_id']  = $plants_id;
        $this->load->view(VIEW_BACK . 'template', $data);
        } else $this->load->view('404');
        } else $this->load->view('404');
    }
        public function harvest_estimation()
    {
        $date_start     = $this->input->post('date_start');
        $harvest_time   = $this->input->post('harvest_time');
        if($date_start!="" && $harvest_time!=""){
        $result         = date('Y-m-d', strtotime($date_start. ' + '.$harvest_time.' days'));
        $data_json      = array('status'=>'200', 'data'=>$result);
        die(json_encode($data_json));
        } else {
            $data_json      = array('status'=>'400');
            die(json_encode($data_json));
        }
    }
    
    function get_realtime_sensor_data($plant_id=""){
        if($plant_id!=""){
        $data_sensor = get_avg_sensor($plant_id);
        if($data_sensor!=false){
        $data_json = array(
            'moisture'=>(INT)$data_sensor->avg_mois,
            'ph'=>number_format((float)$data_sensor->avg_ph, 1, '.', ''),
            'light'=>(INT)$data_sensor->avg_light,
            'temp'=>(INT)$data_sensor->avg_temp,
            'humidity'=>number_format((float)$data_sensor->avg_hum, 2, '.', '')
            );
            $data_json      = array('status'=>'200', 'data'=>$data_json);
            die(json_encode($data_json));
        } else {
            $data_json      = array('status'=>'400');
            die(json_encode($data_json));
        }
        } else {
            $data_json      = array('status'=>'400');
            die(json_encode($data_json));
        }
    }
    
    function device_code_check(){
        $device_code = $this->input->post('device_code');
        $where = array('device_code'=>$device_code);
        $device = $this->device_model->read_device($where);
        $flag = $device->num_rows();
        if($flag){
        //check on projects
            $where = array('device_id'=>$device->row()->id);
            $flag2 = $this->plant_device_model->read_plant_device($where)->num_rows();
            if($flag2==0){
                $data_json      = array('status'=>'200', 'data'=>$device->row()->id);
                die(json_encode($data_json));
            } else {
            $data_json      = array('status'=>'400', 'message'=>'Device is already used!');
            die(json_encode($data_json));
        }
        } else {
            $data_json      = array('status'=>'400', 'message'=>'Device code is unknown!');
            die(json_encode($data_json));
        }
    }
    
    function start_planting_process(){
        auth_redirect();
        $member         = get_current_member();
        $project_name   = $this->input->post('project_name');
        $date_start     = $this->input->post('date_start');
        $mode           = $this->input->post('mode');
        $plants_id      = $this->input->post('plants_id');
        //form validation
        $this->form_validation->set_rules('project_name', 'Project Name', 'required');
        $this->form_validation->set_rules('date_start', 'Date Start', 'required');
        $this->form_validation->set_rules('devices', 'Devices', 'required');
        $devices        = array();
        foreach($this->input->post('devices') as $device_id){
            $where = array('device_code'=>$device_id);
            $device_data = $this->device_model->read_device($where);
            if($device_data->num_rows()!=0)
            $devices[] = $device_data->row()->id;
        }
        if ($this->form_validation->run() == FALSE) { 
        $data = array(
            'member_id'=>$member->id,
            'name'=>$project_name,
            'mode'=>$mode,
            'date_start'=>$date_start,
            'plants_id'=>$plants_id,
            'datecreated'=>date('Y-m-d H:i:s'),
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $flag = $this->plant_model->create_plant($data);
            if($flag){
                //add device 
                foreach($devices as $device_id){
                    $data_device = array(
                        'plant_id'=>$flag,
                        'device_id'=>$device_id,
                        'datecreated'=>date('Y-m-d H:i:s'),
                        'datemodified'=>date('Y-m-d H:i:s')
                    );
                    $this->plant_device_model->create_plant_device($data_device);
                }
                
              //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Add plant successful!</div>');
           redirect(base_url(""));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Add Plant failed! Something went wrong on the database.</div>');
           redirect(base_url(""));
        }} else {
            //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Add Plant failed! Please fill up the form</div>');
           redirect(base_url("start_planting?mode=".$mode."&plants_id=".$plants_id));
        }
    }
    
    public function article()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Article';
        $data['content'] = VIEW_BACK . 'article';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function project_team()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Project Team';
        $data['content'] = VIEW_BACK . 'project_team';
        $member                 = get_current_member();
        $where                  =array('member_id'=>$member->id);
        $plant                  = $this->plant_model->read_plant($where);
        $data['projects_list']  = $plant;
        $member_list            = $this->member_model->read_member();
        $data['member_list']    = $member_list;
        $data['member']         = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function project_team_create() {
        $plant_id         = $this->input->post('plant_id'); 
        $email            = $this->input->post('email');
        $where         = array('email'=>$email);
        $member_check  = $this->member_model->read_member($where);
        if ($member_check->num_rows() !=0){ 
            $member_id     = $member_check->row()->id;
            $where         = array('member_id'=>$member_id, 'plant_id'=>$plant_id);
            $check_device_member_id= $this->device_owner_model->read_device_owner($where);
            if($check_device_member_id->num_rows() ==0){
                $data['plant_id']  = $plant_id;
                $data['member_id'] = $member_id;
                $data['datecreated']    = date("Y-m-d H:i:s");
                $data['datemodified']  = date("Y-m-d H:i:s");
                $flag = $this->device_owner_model->create_device_owner($data);
                if($flag){
                   $data_json = array('status'=>200, 'message'=>'Data Sucess'); 
                    die(json_encode($data_json));
                }
                else{
                    $data_json = array('status'=>400, 'message'=>'Data failed'); 
                    die(json_encode($data_json));
                }
            } else{
                    $data_json = array('status'=>400, 'message'=>'Data failed'); 
                    die(json_encode($data_json));
                }
        }
        else{
                $data_json = array('status'=>400, 'message'=>'Data failed'); 
                die(json_encode($data_json));
            }
    }
    public function water_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Water Chart';
        $data['content'] = VIEW_BACK . 'water_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function farming_days_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Farming Days';
        $data['content'] = VIEW_BACK . 'farming_days_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function soil_moisture_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Soil Moisture';
        $data['content'] = VIEW_BACK . 'soil_moisture_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function soil_ph_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Soil PH';
        $data['content'] = VIEW_BACK . 'soil_ph_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function light_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Light';
        $data['content'] = VIEW_BACK . 'light_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function temprature_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Temprature';
        $data['content'] = VIEW_BACK . 'temprature_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function humadity_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Humadity';
        $data['content'] = VIEW_BACK . 'humadity_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function harvest_estimation_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Harvest Estimation';
        $data['content'] = VIEW_BACK . 'harvest_estimation_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function view_all_chart()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | All Reporting';
        $data['content'] = VIEW_BACK . 'view_all_chart';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function select_project_device()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Select Project';
        $data['content'] = VIEW_BACK . 'select_project_device';
        $member          = get_current_member();
        $data['member']  = $member;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    public function project_detail($plant_id)
    {
        auth_redirect();
        if($plant_id!=""){
            $where = array('id'=>$plant_id);
            $plant           = $this->plant_model->read_plant($where);
            if($plant->num_rows()!=0){
                $plant_data  = $plant->row();
                $where           = array('plants_id', $plant_data->plants_id);
                $plants_data     = $this->plants_model->read_plants($where);
                if($plants_data->num_rows()!=0){
                    $plants          = $plants_data->row();
                    $data['title']   = COMPANY_NAME.' | Project Detail';
                    $data['content'] = VIEW_BACK . 'project_detail';
                    $member          = get_current_member();
                    $data['member']  = $member;
                    $data['plant']   = $plant_data;
                    $date_start      = date('Y-m-d', strtotime($plant_data->datecreated));
                    $harvest_time    = $plants->harvest_time;
                    $harvest_day     = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$harvest_time.' days'));
                    $now_diff        = date('Y-m-d');
                    $datediff        = date_range($harvest_day, $now_diff);
                    $data['harvest_estimation'] = $datediff;
                    $this->load->view(VIEW_BACK . 'template', $data);
                } else $this->load->view('404');
            } else $this->load->view('404');
        } else $this->load->view('404');
    }
    
    public function pre_planting($id=""){
        auth_redirect();
        if($id!=""){
            $where       = array('id'=>$id);
            $plants          = $this->plants_model->read_plants($where);
            if($plants->num_rows()!=0){
                $data['title']   = COMPANY_NAME.' | Pre Planting';
                $data['content'] = VIEW_BACK . 'pre_planting';
                $member          = get_current_member();
                $data['member']  = $member;
                $data['plants']  = $plants->row();
                $this->load->view(VIEW_BACK . 'template', $data);
            } else $this->load->view('404');
        } else $this->load->view('404');
    }
    
    public function select_mode($plants_id){
        if(!empty($plants_id)){
        $data['title']      = COMPANY_NAME.' | Select Mode';
        $data['content']    = VIEW_BACK . 'select_mode';
        $member             = get_current_member();
        $data['member']     = $member;
        $data['plants_id']  = $plants_id;
        $this->load->view(VIEW_BACK . 'template', $data);
        } else $this->load->view('404');
    }
    
    public function employee_create()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Create Employee';
        $data['content'] = VIEW_BACK . 'employee_create';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function employee_edit($id=""){
        auth_redirect();
        if(!empty($id)){
            $where = array('id'=>$id);
            $member_qry = $this->member_model->read_member($where);
            if($member_qry->num_rows()!=0){
            $data['title']   = COMPANY_NAME.' | Employee Edit';
            $data['content'] = VIEW_BACK . 'employee_edit';
            $data['member']  = get_current_member();
            $data['data_edit'] = $member_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
            } else $this->load->view('404');
        } else $this->load->view('404');
    }
    
        function create_employee(){
        $data['name'] = $this->input->post('name'); 
        $data['email'] = $this->input->post('email');
        $data['password'] = hash('md5', $this->input->post('name'));
        $data['profile_picture'] = upload_file('profile_picture', 'images', 'assets/images/user/');
        $data['date_of_birth'] = date('Y-m-d', strtotime($this->input->post('date_of_birth')));
        $data['address'] = $this->input->post('address');
        $data['id_number'] = $this->input->post('id_number');
        $data['type'] = 'employee';
        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['datemodified'] = date('Y-m-d H:i:s');
        $flag = $this->member_model->create_member($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
           redirect(base_url("employee"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("employee"));
       }
    }
    
    function edit_employee(){
        $data['id'] = $this->input->post('id'); 
        $data['name'] = $this->input->post('name'); 
        $data['email'] = $this->input->post('email');
        $data['password'] = hash('md5', $this->input->post('name'));
        $profile_picture = upload_file('profile_picture', 'images','assets/images/user/');
        if($profile_picture!="empty" && $profile_picture!="error" && $profile_picture!="error_extension" && $profile_picture!="error_upload") $data['profile_picture'] = $profile_picture;
        $data['date_of_birth'] = date('Y-m-d', strtotime($this->input->post('date_of_birth')));
        $data['address'] = $this->input->post('address');
        $data['id_number'] = $this->input->post('id_number');
        $data['type'] = 'employee';
        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['datemodified'] = date('Y-m-d H:i:s');
        $flag = $this->member_model->update_member($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Sukses!</div>');
           redirect(base_url("employee"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("employee"));
       }
    }
    
    function employee_delete($id=""){
        $flag = $this->member_model->delete_member($id);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Sukses!</div>');
           redirect(base_url("employee"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("employee"));
       }
    }
//Expenses
    public function expenses()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Expenses';
        $data['content'] = VIEW_BACK . 'expenses';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function expenses_create()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Create expenses';
        $data['content'] = VIEW_BACK . 'expenses_create';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function expenses_edit($id=""){
        auth_redirect();
        if(!empty($id)){
            $where = array('id'=>$id);
            $member_qry = $this->expenses_model->read_expenses($where);
            if($member_qry->num_rows()!=0){
            $data['title']   = COMPANY_NAME.' | Expenses Edit';
            $data['content'] = VIEW_BACK . 'expenses_edit';
            $data['member']  = get_current_member();
            $data['data_edit'] = $member_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
            } else $this->load->view('404');
        } else $this->load->view('404');
    }
    
        function create_expenses(){
        $data['nominal'] = $this->input->post('nominal'); 
        $data['category'] = $this->input->post('category');
        $data['description'] = $this->input->post('description');
        $data['picture'] = upload_file('picture', 'images', 'assets/images/expenses/');
        $data['type'] = $this->input->post('type');
        $data['date'] = date('Y-m-d', strtotime($this->input->post('date')));
        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['datemodified'] = date('Y-m-d H:i:s');
        $flag = $this->expenses_model->create_expenses($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
           redirect(base_url("expenses"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("expenses"));
       }
    }
    
    function edit_expenses(){
        $data['id'] = $this->input->post('id'); 
        $data['nominal'] = $this->input->post('nominal'); 
        $data['category'] = $this->input->post('category');
        $data['description'] = $this->input->post('description');
        $data['type'] = $this->input->post('type');
        $data['date'] = date('Y-m-d', strtotime($this->input->post('date')));
        $data['datemodified'] = date('Y-m-d H:i:s');
        $picture = upload_file('picture', 'images','assets/images/expenses/');
        if($picture!="empty" && $picture!="error" && $picture!="error_extension" && $picture!="error_upload") $data['picture'] = $picture;
        $flag = $this->expenses_model->update_expenses($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Sukses!</div>');
           redirect(base_url("expenses"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("expenses"));
       }
    }
    
    function expenses_delete($id=""){
        $flag = $this->expenses_model->delete_expenses($id);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Sukses!</div>');
           redirect(base_url("expenses"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("expenses"));
       }
    }
    
    //Inventory
    public function inventory()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | inventory';
        $data['content'] = VIEW_BACK . 'inventory';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function inventory_create()
    {
        auth_redirect();
        $data['title']   = COMPANY_NAME.' | Create inventory';
        $data['content'] = VIEW_BACK . 'inventory_create';
        $data['member']  = get_current_member();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function inventory_edit($id=""){
        auth_redirect();
        if(!empty($id)){
            $where = array('id'=>$id);
            $member_qry = $this->inventory_model->read_inventory($where);
            if($member_qry->num_rows()!=0){
            $data['title']   = COMPANY_NAME.' | inventory Edit';
            $data['content'] = VIEW_BACK . 'inventory_edit';
            $data['member']  = get_current_member();
            $data['data_edit'] = $member_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
            } else $this->load->view('404');
        } else $this->load->view('404');
    }
    
        function create_inventory(){
        $data['name'] = $this->input->post('name');
        $data['price'] = $this->input->post('price'); 
        $data['description'] = $this->input->post('description');
        $data['date_purchase'] = date('Y-m-d', strtotime($this->input->post('date')));
        $data['picture'] = upload_file('picture', 'images', 'assets/images/inventory/');
        $data['category'] = $this->input->post('category');
        $data['datecreated'] = date('Y-m-d H:i:s');
        $data['datemodified'] = date('Y-m-d H:i:s');
        $flag = $this->inventory_model->create_inventory($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
           redirect(base_url("inventory"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("inventory"));
       }
    }
    
    function edit_inventory(){
        $data['id'] = $this->input->post('id'); 
        $data['name'] = $this->input->post('name');
        $data['price'] = $this->input->post('price'); 
        $data['description'] = $this->input->post('description');
        $data['date_purchase'] = date('Y-m-d', strtotime($this->input->post('date')));
        $data['picture'] = upload_file('picture', 'images', 'assets/images/inventory/');
        $data['category'] = $this->input->post('category');
        $data['datemodified'] = date('Y-m-d H:i:s');
        $picture = upload_file('picture', 'images','assets/images/inventory/');
        if($picture!="empty" && $picture!="error" && $picture!="error_extension" && $picture!="error_upload") $data['picture'] = $picture;
        $flag = $this->inventory_model->update_inventory($data);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Sukses!</div>');
           redirect(base_url("inventory"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengubahan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("inventory"));
       }
    }
    
    function inventory_delete($id=""){
        $flag = $this->inventory_model->delete_inventory($id);
        if ($flag) {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Sukses!</div>');
           redirect(base_url("inventory"));
       } else {
           //set flashdata
           $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penghapusan Data Gagal! Terjadi kesalahan pada database.</div>');
           redirect(base_url("inventory"));
       }
    }
    
    function logout(){
        session_destroy();
        redirect(base_url());
    }

        function login()
    {
      $member_login = $this->session->userdata('member_login');
        if (empty($member_login)){
            $data['title'] = COMPANY_NAME.' | Login';
            $this->load->view(VIEW_BACK . 'login', $data);
        }
        else
            redirect(base_url('/'));
    }
    
    function login_process()
    {
        $username    = $this->input->post('username');
        $password = hash('md5', $this->input->post('password'));
        $where    = array(
            'username'=>$username,
            'password'=>$password
        );
        $flag     = $this->member_model->read_member($where)->num_rows();
        if ($flag) {
            $this->session->set_userdata('member_login', $username);
            $this->session->set_userdata('lang_admin', 'en');
            $data = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success"><strong>Sukses!</strong> Sesaat lagi anda akan masuk..</div>'
            );
            die(json_encode($data));
            redirect(base_url(''));
        } else {
            $data = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger"><strong>Error!</strong> Login Gagal! Silahkan Cek Email/Password Anda!</div>'
            );
            die(json_encode($data));
        }
    }
    
    function upload_file_summernote()
        {
         $file = upload_file("file", "images", 'assets/img/pages/');
        if($file!="empty"){
            echo base_url($file);
        } else
            {
              echo  $message = 'Ooops!  Your upload triggered the following error:  '.$file;
            }

        }

}

?>