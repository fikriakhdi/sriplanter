<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                = 'backend';
$route['404_override']                      = '';
$route['translate_uri_dashes']              = FALSE;

/*
ROUTE BACKEND
*/
$route['dashboard']                         = 'backend/';
$route['add_plant']                         = 'backend/add_plant';
$route['devices']                           = 'backend/devices';
$route['add_device']                        = 'backend/add_device';
$route['projects']                          = 'backend/projects';
$route['select_project']                    = 'backend/select_project';
$route['detail_report']                     = 'backend/detail_report';
$route['tips_trick']                        = 'backend/tips_trick';
$route['start_planting']                    = 'backend/start_planting';
$route['article']                           = 'backend/article';
$route['pre_planting/(:num)']               = 'backend/pre_planting/$1';
$route['select_mode/(:num)']                = 'backend/select_mode/$1';
$route['water_chart']                       = 'backend/water_chart';
$route['farming_days_chart']                = 'backend/farming_days_chart';
$route['soil_ph_chart']                     = 'backend/soil_ph_chart';
$route['light_chart']                       = 'backend/light_chart';
$route['temprature_chart']                  = 'backend/temprature_chart';
$route['soil_moisture_chart']               = 'backend/soil_moisture_chart';
$route['humidity_chart']                    = 'backend/humidity_chart';
$route['view_all_chart']                    = 'backend/view_all_chart';
$route['project_detail/(:num)']             = 'backend/project_detail/$1';
$route['harvest_estimation_chart']          = 'backend/harvest_estimation_chart';
$route['create_projects']                   = 'backend/projects_create';
$route['select_project_device']             = 'backend/select_project_device';
$route['project_team_edit']                 = 'backend/project_team_edit';
$route['projects_edit/(:num)']              = 'backend/projects_edit/$1';
$route['projects_delete/(:num)']            = 'backend/projects_delete/$1';
$route['project_team']                      = 'backend/project_team';
$route['create_employee']                   = 'backend/employee_create';
$route['employee_edit/(:num)']              = 'backend/employee_edit/$1';
$route['employee_delete/(:num)']            = 'backend/employee_delete/$1';
$route['expenses']                          = 'backend/expenses';
$route['create_expenses']                   = 'backend/expenses_create';
$route['expenses_edit/(:num)']              = 'backend/expenses_edit/$1';
$route['expenses_delete/(:num)']            = 'backend/expenses_delete/$1';
$route['inventory']                         = 'backend/inventory';
$route['create_inventory']                  = 'backend/inventory_create';
$route['inventory_edit/(:num)']             = 'backend/inventory_edit/$1';
$route['inventory_delete/(:num)']           = 'backend/inventory_delete/$1';
$route['timeline']                          = 'backend/timeline';
$route['performances']                      = 'backend/performances';
$route['contract']                          = 'backend/contract';
$route['payment']                           = 'backend/payment';
$route['login']                             = 'backend/login';
$route['logout']                            = 'backend/logout';

