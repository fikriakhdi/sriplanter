<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pekerja</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('user_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/pekerja_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_pekerja;?>">
                            <div class="form-group">
                              <label>User<span style="color:#f00">*</span></label>
                            <select class="form-control" name="id_user" id="id_user" required>
                              <?php
                              $data_list = get_all_member_list();
                              if($data_list!=false){
                                echo '<option>Pilih User</option>';
                                foreach($data_list->result() as $data){
                                  echo '<option value="'.$data->id_user.'">'.$data->username.'</option>';
                                }
                              } else echo '<option>Maaf tidak ada pilihan user</option>';
                              ?>
                            </select>
                            <script>
                            $("#id_user").val("<?php echo $data_edit->id_user;?>").change();
                            </script>
                          </div>
                          <div class="form-group">
                            <label>Nama<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control" name="nama_pekerja" placeholder="Ketik Nama" maxlength="150"  value="<?php echo $data_edit->nama_pekerja;?>" required>
                          </div>
                          <div class="form-group">
                            <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control" name="alamat_pekerja" placeholder="Ketik Alamat"  value="<?php echo $data_edit->alamat_pekerja;?>" required>
                          </div>
                          <div  class="form-group">
                            <label>Telp<span style="color:#f00">*</span></label>
                            <input type="text" class="form-control" name="telp_pekerja" placeholder="Ketik No Telp"  value="<?php echo $data_edit->telp_pekerja;?>" required>
                          </div>
                          <div  class="form-group">
                            <label>Gender<span style="color:#f00">*</span></label>
                            <select class="form-control" name="gender_pekerja" id="gender_pekerja"  value="<?php echo $data_edit->gender_pekerja;?>" required>
                              <option>Pilih Gender</option>
                              <option value="pria">Pria</option>
                              <option value="wanita">Wanita</option>
                            </select>
                            <script>
                            $("#gender_pekerja").val("<?php echo $data_edit->gender_pekerja;?>").change();
                            </script>
                          </div>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
