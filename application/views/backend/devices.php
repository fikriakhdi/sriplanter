<!--open content add divice-->
<div class="pos-title">
    <h3>Devices</h3>
</div>
<div class="content-add-device" id="add_device">
    <div class="head-button-pos">
        <a href="<?php echo base_url('add_device');?>"><span class="fa fa-plus"></span>Add Device</a>
    </div>

    <div class="pos-data-table" id="device_data_table">
        <table class="table datatable table-striped table-bordered">
            <thead>
            <tr>
                <th>Device ID</th>
                <th>Project Name</th>
                <th>Day Used</th>
<!--                <th>SSID</th>-->
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                <?php 
                $devices = get_devices_list($member->id);
                if($devices!=false){
                    foreach($devices->result() as $device){
                        $status = get_device_status($device->device_id);
                ?>
            <tr class="des-table">
                
                <td><?php echo $device->device_code;?></td>
                <td><?php echo $device->name;?></td>
                <td><?php echo date_range($device->date_created, date('Y-m-d'));?> Days</td>
<!--                <td>My Wife Outdors</td>-->
                <td class="<?php echo strtolower($status);?>-device"><?php echo $status;?></td>
            </tr>
                <?php  } }?>
                </tbody>
        </table>
    </div>
</div>
<!--close content add device-->