<!--open content select project-->
<div class="pos-title">
    <h3>Select Project</h3>
</div>
<div class="content-select-project" id="select-project">
<!--
    <div class="head-button-pos">
        <a href="<?php echo base_url('add_device');?>"><span class="fa fa-plus"></span>Add Device</a>
    </div>
-->
    <div class="col-md-12 pos-content-select-pro">
        <?php 
        $projects = get_project_list("", $member->id);
        if($projects!=false){
            foreach($projects->result() as $project){
            ?>
        <div class="col-md-3 cont-select-pro">
            <a href="<?php echo base_url('project_detail/'.$project->id);?>">
                <div class="col-select-pro">
                    <div class="head">
                        <img class="" src="<?php echo base_url($project->plants_picture);?>">
                    </div>
                    <div class="foot">
                        <h4><?php echo $project->plants_name;?></h4>
                    </div>
                </div>
            </a>
        </div>
        <?php 
            } }
            ?>
    </div>
<!--close content select project-->