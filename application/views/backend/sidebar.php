
<!-- SIDEBAR -->
<aside class="site-sidebar scrollbar-enabled clearfix">
    <!-- User Details -->
    <div class="side-user">
        <a class="col-sm-12 media clearfix" href="javascript:void(0);">
            <figure class="media-left media-middle user--online thumb-sm mr-r-10 mr-b-0">
                <img src="<?php echo base_url($member->profile_picture);?>" class="media-object rounded-circle" alt="">
            </figure>
            <div class="media-body hide-menu">
                <h4 class="media-heading mr-b-5 text-uppercase"><?php echo $member->name;?></h4>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
    <!-- /.side-user -->
    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <ul class="nav in side-menu">
          <li><a href="<?php echo base_url('');?>" class="ripple"><i class="list-icon material-icons">dashboard</i> <span class="hide-menu">Dashboard</span></a></li>
          <li><a href="<?php echo base_url('add_plant');?>" class="ripple"><i class="list-icon material-icons">local_florist</i> <span class="hide-menu">Plants</span></a></li>
          <li><a href="<?php echo base_url('devices');?>" class="ripple"><i class="list-icon material-icons">devices</i> <span class="hide-menu">Devices</span></a></li>
          <li><a href="<?php echo base_url('select_project');?>" class="ripple"><i class="list-icon material-icons">insert_chart</i> <span class="hide-menu">Report</span></a></li>
          <li><a href="<?php echo base_url('projects');?>" class="ripple"><i class="list-icon material-icons">business_center</i><span class="hide-menu"> Projects</span></a></li>
          <li><a href="<?php echo base_url('project_team');?>" class="ripple"><i class="list-icon material-icons">next_week</i><span class="hide-menu"> Project Team</span></a></li>
          <li><a href="<?php echo base_url('tips_trick');?>" class="ripple"><i class="list-icon material-icons">class</i> <span class="hide-menu">Tips & Tricks</span></a></li>

        <li><a href="<?php echo base_url('logout');?>"><i class="list-icon material-icons">settings_power</i> <span class="hide-menu">Sign Out</span></a>
        </li>
        </ul>
        <!-- /.side-menu -->
    </nav>
    <!-- /.sidebar-nav -->
</aside>
<!-- /.site-sidebar -->