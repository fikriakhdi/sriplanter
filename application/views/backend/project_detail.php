<!--open content real_time_monitoring_project-->
<div class="pos-title">
    <h3><?php echo $plant->name;?></h3>
</div>
<div class="content-detile-project-realtim" id="detile-project-realtime">
    
    <div class="pos-head-button-chart">
        <div class="left">
            <div class="form-group form-group-custom-detile-pro">
                <select name="carlist" form="carform" class="custom-select-pro-detail">
                  <option value="Weekly1">All Device</option>
                  <option value="Weekly2">SRI0001</option>
                  <option value="Weekly3">SRI0002</option>
                  <option value="Weekly4">SRI0003</option>
                    <option value="Weekly5">SRI0004</option>
                </select>
            </div>
            <div class="form-group form-group-custom-detile-pro">
                <select name="carlist" form="carform" class="custom-select-pro-detail">
                  <option value="Weekly1">Real time report</option>
                  <option value="Weekly2">Daily report average</option>
                  <option value="Weekly3">Weekly report average</option>
                  <option value="Weekly4">Monthly report average</option>
                </select>
            </div>
        </div>
        <div class="right">
            <a href="<?php echo base_url('view_all_chart');?>" class="open-all-report">Open Chart</a>
        </div>
    </div>
    <div class="pos-cont-detile-pro">
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('water_chart');?>">
                <div class="pos-col-detile-pro col-detile-pro-blue">
                    <i class="fas fa-water"></i>
                    <div class="head">
                        <h4>Water Usage</h4>
                    </div>
                    <div class="body">
                        <h3><?php echo get_total_water_usage($plant->id, 'plant_id');?></h3><span>litre</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('farming_days_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-orange">
                <i class="fas fa-calendar-alt"></i>
                <div class="head">
                    <h4>Farming Days</h4>
                </div>
                <div class="body">
                    <h3><?php echo get_total_farming_days($plant->id, "plant_id");?></h3><span>Days</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('soil_moisture_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-pink">
                <i class="fas fa-tint"></i>
                <div class="head">
                    <h4>soil moisture</h4>
                </div>
                <div class="body">
                    <h3 id="moisture">0</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('soil_ph_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue-sky">
                <i class="fas fa-thermometer"></i>
                <div class="head">
                    <h4>soil ph</h4>
                </div>
                <div class="body">
                    <h3 id="ph">0</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('light_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue">
                <i class="fas fa-sun"></i>
                <div class="head">
                    <h4>light</h4>
                </div>
                <div class="body">
                    <h3 id="light">0</h3>
                    <span>LUX</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('temprature_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-orange">
                <i class="fas fa-thermometer-three-quarters"></i>
                <div class="head">
                    <h4>temperature</h4>
                </div>
                <div class="body">
                    <h3 id="temperature">0</h3>
                    <strong>&#176;</strong><span>C</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('humidity_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-pink">
                <i class="fas fa-wind"></i>
                <div class="head">
                    <h4>humidity</h4>
                </div>
                <div class="body">
                    <h3 id="humidity">0</h3>
                    <span>%</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('harvest_estimation_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue-sky">
                <i class="fab fa-pagelines"></i>
                <div class="head">
                    <h4>harvest estimation</h4>
                </div>
                <div class="body">
                    <h3><?php echo $harvest_estimation;?></h3>
                    <span>days left</span>
                </div>
            </div>
            </a>
        </div>
    </div>
</div>
<!--close content detile project-->
<script>
$(document).ready(function(){
     $.getJSON( '<?php echo base_url('backend/get_realtime_sensor_data/'.$plant->id);?>', function(data) {
 $("#humidity").text(data.data.humidity);
 $("#ph").text(data.data.ph);
 $("#moisture").text(data.data.moisture);
 $("#temperature").text(data.data.temp);
 $("#light").text(data.data.light);
});

    
    
});
    (function(){
    // do some stuff
    setInterval(function(){
    $.getJSON( '<?php echo base_url('backend/get_realtime_sensor_data/'.$plant->id);?>', function(data) {
 $("#humidity").text(data.data.humidity);
 $("#ph").text(data.data.ph);
 $("#moisture").text(data.data.moisture);
 $("#temperature").text(data.data.temp);
 $("#light").text(data.data.light);
});}
        , 2000);
})();
        
</script>