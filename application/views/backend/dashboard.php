           
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.css');?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.print.min.css');?>' rel='stylesheet' media='print' />
<script src='<?php echo base_url('assets/plugins/fullcalendar/lib/moment.min.js');?>'></script>
<script src='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.js?1');?>'></script>
<!-- <script src='<?php //echo base_url('assets/js/event_calendar.js');?>'></script> -->
<link href="<?php echo base_url('assets/css/event_calendar.css');?>" rel="stylesheet">
<!-- Page Title Area -->
<!--open content add divice-->
    <div class="pos-title">
        <h3>Dashboard <span>statistics, charts, events and reports</span></h3>
    </div>
    <div class="content-add-device" id="add_device">
        <div class="col-md-12">
        <?php
            $message = $this->session->userdata('message');
            if(!empty($message)) echo $message;
        ?>
        </div>
        <div class="head-button-pos">
            <a class="btn btn-primary" href="<?php echo base_url('add_plant');?>"><span class="fa fa-plus"></span> Add Plants</a>
        </div>
                <!-- Counters -->
        <?php 
        $projects = get_project_list(8, $member->id);
        if($projects!=false){ 
            foreach($projects->result() as $project){
        ?>
        <div class="col-md-3 cont-select-pro">
            <a href="<?php echo base_url('project_detail/'.$project->id);?>">
                <div class="col-select-pro">
                    <div class="head">
                        <img class="" src="<?php echo base_url($project->plants_picture);?>" alt="Tomato">
                    </div>
                    <div class="footlong">
                        <h4><?php echo $project->name;?></h4>
                    </div>
                </div>
            </a>
        </div>
        <?php 
            }
        } else {
        ?>
                <div class="">
                    <div class="col-md-12  stitched centered-text med-size vertically-text bold-text div-link" id="project_list" href="<?php echo base_url('add_plant');?>">
                    <p>You don't have project yet, grow now</p>
                    </div>
                </div>
        <?php } ?>
                <!-- /.row -->
                <!-- Chart Group 1 -->
                
                <div class="pos-cont-detile-pro">
                    <div class="col-md-3 col-detile-pro ">
                            <div class="pos-col-detile-pro col-detile-pro-blue">
                                <i class="fas fa-code-branch"></i>
                                <div class="head">
                                    <h4>Total Devices</h4>
                                </div>
                                <div class="body">
                                    <h3 class="counter"><?php echo get_total_devices($member->id);?></h3><span>Devices</span>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3 col-detile-pro ">
                        <div class="pos-col-detile-pro col-detile-pro-orange">
                            <i class="fas fa-leaf"></i>
                            <div class="head">
                                <h4>Total Plants</h4>
                            </div>
                            <div class="body">
                                <h3 class="counter"><?php echo get_total_plants($member->id);?></h3><span>Plants</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-detile-pro ">
                        <div class="pos-col-detile-pro col-detile-pro-pink">
                            <i class="fas fa-water"></i>
                            <div class="head">
                                <h4>Water Usage</h4>
                            </div>
                            <div class="body">
                                <h3 class="counter"><?php echo get_total_water_usage($member->id, "member_id");?></h3><span>Litre</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-detile-pro ">
                        <div class="pos-col-detile-pro col-detile-pro-blue-sky">
                            <i class="fas fa-calendar-alt"></i>
                            <div class="head">
                                <h4>Farming Days</h4>
                            </div>
                            <div class="body">
                                <h3 class="counter"><?php echo get_total_farming_days($member->id, "member_id");?></h3><span>Days</span>
                            </div>
                        </div>
                    </div>

                </div>
<!--
                	<div style="col-md-12 col-sm-12 widget-holder widget-full-height">
		<canvas id="canvas"></canvas>
	</div>
-->
	<br>
	<br>
</div>
<script>
$(document).ready(function() {

		$('#calendar').fullCalendar({
			defaultDate: '2019-01-01',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
					{
						title: 'First day work',
						start: '2019-01-01',
						end: '2019-01-01',
						url: 'http://wsp.jualrumah99.com',
						disableDragging: true,
						editable: false
					}
			]
		});

	});

</script>