<!--open content add plant-->
<div class="pos-title">
    <h3>Add Plant</h3>
</div>
<div class="content-select-project" id="select-project">
    <div class="col-md-12 pos-content-select-pro">
        <div class="pos-title-sub">
            <h3>Select Your Plant</h3>
        </div>
        <?php
                    $plants = get_all_plants_list();
                    if($plants!=false){
                        foreach($plants->result() as $item){
                    ?>
        <div class="col-md-3 cont-select-pro">
            <a href="<?php echo base_url('pre_planting/'.$item->id);?>">
                <div class="col-select-pro">
                    <div class="head">
                        <img class="" src="<?php echo base_url($item->picture);?>" alt="Tomato">
                    </div>
                    <div class="foot2">
                        <h4><?php echo $item->name;?></h4>
                    </div>
                </div>
            </a>
        </div>
        <?php }} else echo 'Sorry, There\'s no plants to choose.';?>
</div>
<!--close content add plant-->

