<!--open cntentn add device-->
<div class="pos-title">
    <h3>Add Device</h3>
</div>
<div class="content-add-device" id="add_device">
    <div class="head-button-pos">
        <a href="#">Scan Your Device</a>
    </div>
    <div class="pos-add-device-content">
        <div class="pos-col-add-device-more" id="synchronize_device">
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_1">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="1">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_1">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="1" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_1">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_2">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="2">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_2">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="2" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_2">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_3">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="3">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_3">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="3" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_3">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_4">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="4">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_4">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="4" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_4">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_5">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="5">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_5">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="5" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_5">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_6">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="6">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_6">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="6" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_6">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_7">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="7">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_7">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="7" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_7">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_8">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="8">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_8">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="8" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_8">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_9">
                            <img class="image" src="http://sriplanter.jualrumah99.com/assets/img/plants/lettuce.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="9">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_9">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="9" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" id="value_device_9">
                    </div>
                </div>
    </div>
     <div class="pos-btn-select-device">
            <a class="btn-orange-suctom" href="<?php echo base_url('select_project_device');?>" class="">select project</a>
        </div>
</div>
<!--close cntentn add device-->

  <div class="modal fade" id="AddDevice" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Device</h4>
        </div>
        <div class="modal-body">
          <form>
              <div class="form-group form-group-custom">
                  <input type="text" class="form-control form-control-custom " placeholder="Inut Device Code" id="device_id" name="device_id">
              </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn-save-custom" data-dismiss="modal">SAVE DEVICE</button>
            <button type="button" class="btn-cancle-custom" data-dismiss="modal">CANCLE</button>
        </div>
      </div>
      
    </div>
  </div>