<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-user"style="padding-right:10px"></span> Redeem</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="redeem_list_menu" href="#redeeem_list" class="active" data-toggle="tab">Redeem List</a> </li>
         <li> <a id="add_redeem_menu" href="#add_redeem" data-toggle="tab"> Add Redeem</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="redeeem_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Pelanggan</th>
              <th>Total Poin</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_redeem_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_pelanggan;?></td>
              <td><?php echo "-".$data->total_point;?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/redeem_edit/'.$data->id_point);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_redeem/'.$data->id_point);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_redeem">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_redeem');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label for="nama_Penjualan">Pelanggan<span style="color:#f00">*</span></label>
          <select name="id_pelanggan" class="form-control">
            <?php
            $pelanggan_list = get_all_pelanggan_list();
            if($pelanggan_list!=false){
              echo '<option>Pilih Pelanggan</option>';
              foreach($pelanggan_list->result() as $pelanggan){
                echo '<option value="'.$pelanggan->id_pelanggan.'">'.$pelanggan->nama_pelanggan.'</option>';
              }
            } else echo '<option>Tidak ada pilihan pelanggan</option>';
            ?>
          </select>
        </div>
        <div class="form-group">
          <label for="no_telepon">Besar Point<span style="color:#f00">*</span></label>
          <input type="number" class="form-control" id="total_point" name="total_point" aria-describedby="emailHelp" placeholder="Ketik Poin"   value="" required>
        </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
