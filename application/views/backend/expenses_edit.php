
<link href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css');?>" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
          <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                  <h5 class="mr-0 mr-r-5">Pengeluaran</h5>
                  <p class="mr-0 text-muted d-none d-md-inline-block">Mengatur semua data pengeluaran</p>
              </div>
          </div>
          <!-- /.page-title -->
          <!-- =================================== -->
          <!-- Different data widgets ============ -->
          <!-- =================================== -->
          <div class="widget-list">
              <div class="row">
                <div class="col-md-12">
                  <?php
                  echo (!empty($this->session->flashdata('message'))?$this->session->flashdata('message'):'');
                   ?>
                 </div>
                 <div class="col-md-12" id="top_button">
                   <a class="btn btn-primary btn-rounded" href="<?php echo base_url('expenses');?>"><span class="fa fa-arrow-left"></span> Kembali</a>
                 </div>
                 <div class="col-md-12 widget-holder widget-full-height">
                   <div class="widget-bg">
                       <div class="widget-heading clearfix">
                           <h5>Create expenses</h5>
                           <!-- /.widget-actions -->
                       </div>
                       <!-- /.widget-heading -->
                       <div class="widget-body clearfix">
                          <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/edit_expenses');?>" enctype="multipart/form-data">
                              <input name="id" type="hidden" value="<?php echo $data_edit->id;?>">
                              <div class="form-group">
                                <label>Nominal<span style="color:#f00">*</span></label>
                                <input type="number" class="form-control" name="nominal" placeholder="Ketik Nominal" value="<?php echo $data_edit->nominal;?>" required>
                              </div>
                              <div class="form-group">
                                <label>Kategori<span style="color:#f00">*</span></label>
                                <select name="category" id="category" class="form-control" required>
                                    <option>Pilih Kategori</option>
                                    <option value="salary">Gaji</option>
                                    <option value="oprational">Oprasional</option>
                                    <option value="assets">Aset</option>
                                    <option value="transportation">Transportasi</option>
                                    <option value="entertainment">Hiburan</option>
                                    <option value="meals">Makanan</option>
                                    <option value="production_cost">Ongkos Produksi</option>
                                  </select>
                              </div>
                              <script>
                              $(document).ready(function(){
                                 $("#category").val('<?php echo $data_edit->category;?>').change(); 
                              });
                              </script>
                              <div class="form-group">
                                <label>Deskripsi<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control" name="description" placeholder="Ketik Deskripsi" maxlength="150"  value="<?php echo $data_edit->description;?>" required>
                              </div>

                              <div class="form-group">
                                <label>Foto<span style="color:#f00">*</span></label>
                              <div class="venue-pictures">
                                  <img id="file" class="img-thumbnail img-responsive change_picture" data-file="profile_picture" src="<?php echo ($data_edit->picture==""?base_url('assets/images/no-image.jpg'):base_url($data_edit->picture));?>">
                                  <input type="file" class="form-control file_input_logo" name="picture" id="profile_picture" onchange="imagepreview(this, 'file')">
                                </div>
                              </div>
                              <div class="form-group">
                                <label>Tanggal<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control date_picker" name="date" placeholder="Pilih Tanggal"  value="<?php echo date('m/d/Y', strtotime($data_edit->date));?>" required>
                              </div>
                              <div class="form-group">
                                <label>Tipe<span style="color:#f00">*</span></label>
                               <select name="type" id="type" class="form-control" required>
                                    <option>Pilih Tipe</option>
                                    <option value="fixed">Fixed</option>
                                    <option value="variable">Variable</option>
                                  </select>
                              </div>
                              <script>
                              $(document).ready(function(){
                                 $("#type").val('<?php echo $data_edit->type;?>').change(); 
                              });
                              </script>
                              <div class="footer-form">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                          </form>

                       </div>
                       <!-- /.widget-body -->
                   </div>
                 </div>

        </div>
      </div>
    <!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
