<!--open cntentn FARMING DAYS chart-->
<div class="pos-title">
    <h3>FARMING DAYS - TOMATO 1</h3>
</div>
<div class="content-farming-days" id="farming_days">
    <div class="pos-head-button-chart">
        <div class="left">
            <div class="form-group">
                <select name="carlist" form="carform">
                  <option value="Weekly1">Weekly 1</option>
                  <option value="Weekly2">Weekly 2</option>
                  <option value="Weekly3">Weekly 3</option>
                  <option value="Weekly4">Weekly 4</option>
                <option value="Weekly5">Weekly 5</option>
                </select>
            </div>
        </div>
        <div class="right">
            <button class="open-all-report" onclick="">EXPORT TO CSV</button>
        </div>
    </div>
    <div class="pos-body-chart">
        <div class="content-chart" id="farming_days_chart"></div>
    </div>
</div>
<!--close cntentn water chart-->