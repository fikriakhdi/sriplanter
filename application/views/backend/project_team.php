<!--open content project team-->
<div class="pos-title">
    <h3>Project Team</h3>
</div>
<div class="content-add-device" id="add_device">
        <div class="head-button-pos">
        <button type="button" data-toggle="modal" data-target="#AddTeam"><span class="fa fa-plus"></span>Add Team</button>
    </div>
    <div class="pos-data-table table-responsive data-table-project-for-pc" id="projec_persons_data_table">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Project Name</th> 
                    <th>Member Name</th>
                    <th>Member Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $project_owner= get_all_project_team($member->id);
                        if ($project_owner !=false) {
                            foreach ($project_owner->result() as $data_project_owner){
                            ?>
                            <tr class="des-table">
                                <td><?php echo $data_project_owner->plant_name;?></td>
                                <td><?php echo $data_project_owner->member_name;?></td>
                                <td><?php echo $data_project_owner->member_email;?></td>
                                <td>
                                    <div class="pos-btn-collapse">
                                        <div class="left">
                                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i></a>
                                          </div>
                                          <div class="right">
                                              <a class="" href="<?php echo base_url('project_team_edit');?>"><i class="list-icon material-icons">edit</i></a>
                                          </div>
                                    </div>
                                </td>
                            </tr>
                    <?php }} ;?>
            </tbody>
        </table>
<!--
        <div class="pos-btn-select">
            <button onclick="" class="button select-btn" type="button">Delete</button>
        </div>
-->
    </div>
    <div class="data-table-project-for-mobile">
        <div class="pos-con-data-for-mobile-project">
            <div class="wrapper">
              <div class="half">
                <div class="tab blue">
                  <input id="tab-four" type="radio" name="tabs2">
                  <label for="tab-four">Project 1</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-five" type="radio" name="tabs2">
                  <label for="tab-five">Project 2</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-six" type="radio" name="tabs2">
                  <label for="tab-six">Project 3</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                  <div class="tab blue">
                  <input id="tab-four" type="radio" name="tabs2">
                  <label for="tab-four">Project 4</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-five" type="radio" name="tabs2">
                  <label for="tab-five">Project 5</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-six" type="radio" name="tabs2">
                  <label for="tab-six">Project 6</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Member Name</td>
                            <td class="node">:</td>
                            <td class="right">Sri mulyani</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Member Email</td>
                            <td class="node">:</td>
                            <td class="right">srimulayni@sriplant.com</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href=""><i class="list-icon material-icons">highlight_off</i>Delete</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!--close content add device-->

<!--open modal-->
  <div class="modal fade" id="AddTeam" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Team</h4>
        </div>
        <div class="modal-body">
              <form class="" method="post" action="<?php echo base_url('backend/project_team_create');?>" id="create-pro-team">
                <div class="form-group">
                    <label>Select Project</label>
                    <select class="select-control-custom" name="plant_id">
                         <option>Choose your project</option>
                    <?php 
                    if ($projects_list->num_rows()!=0) {
                        foreach ($projects_list->result() as $projects_list_content){
                    ?>

                      <option value="<?php echo $projects_list_content->id;?>"><?php echo $projects_list_content->name;?></option>
                    <?php }} ?>
                        </select>
                </div>
                <div class="form-group">
                    <label>Add Member Email</label>
                    <input type="text" class="select-control-custom" placeholder="Sri@sriplanting" value="" name="email" id="email">
<!--
                    <select class="select-control-custom" name="carlist" form="carform">
                        <option>Select Member</option>
                        <?php
                        if($member_list->num_rows()!=0){
                            foreach ($member_list->result() as $member_list_content) {
                        ?>
                            <option value="<?php echo $member_list_content->id;?>"><?php echo $member_list_content->email;?></option>
                        <?php }} ?>
                    </select>
-->
                </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn-save-custom" id="submit-add-team">SAVE DEVICE</button>
                    <button type="button" class="btn-cancle-custom" data-dismiss="modal">CANCLE</button>
                </div>
            </form>
        </div>
        
      </div>
      
    </div>
  </div>
<!--close modal-->