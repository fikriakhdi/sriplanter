           
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.css');?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.print.min.css');?>' rel='stylesheet' media='print' />
<script src='<?php echo base_url('assets/plugins/fullcalendar/lib/moment.min.js');?>'></script>
<script src='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.js?1');?>'></script>
<!-- <script src='<?php //echo base_url('assets/js/event_calendar.js');?>'></script> -->
<link href="<?php echo base_url('assets/css/event_calendar.css');?>" rel="stylesheet">
<!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h5 class="mr-0 mr-r-5">Add Plant</h5>
                </div>
            </div>
            <!-- /.page-title -->
            <!-- =============================== ==== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <!-- Counters -->
                <!-- /.row -->
                <!-- Chart Group 1 -->
                <div class="row">
                    <div class="col-md-12">
                    <h6>SELECT MODE : </h6>
                    </div>

                        <div class="col-md-2 col-sm-6 widget-holder widget-full-height select-mode pos-mobile-select-mode">
                            <a href="#" class="show-element" data-target="#sri_planting" data-status="show">
                        <div class="card bg-color-scheme" >
                          <div class="card-body card-body-oscar-custom">
                          <img class="card-img-top" src="<?php echo base_url('assets/img/plants/Icon-planting-01.png');?>">
                              <br>
                            <h5 class="card-title" style="text-align:center">Sri Planting</h5>
                          </div>
                        </div>
                            </a>
                            </div>
                    <div class="col-md-2 col-sm-6 widget-holder widget-full-height select-mode pos-mobile-select-mode">
                        <a href="#" class="show-element" data-target="#pro_planting" data-status="hide">
                        <div class="card orange-bg" >
                          <div class="card-body card-body-oscar-custom">
                          <img class="card-img-top" src="<?php echo base_url('assets/img/plants/Icon-planting-02.png');?>">
                              <br>
                            <h5 class="card-title" style="text-align:center">Pro Planting</h5>
                          </div>
                        </div>
                        </a>
                            </div>
                    <div class="col-md-12 active-element widget-bg" id="sri_planting" >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br><br>
                                <a href="<?php echo base_url('start_planting?mode=sri_planting&plants_id='.$plants_id);?>" class="btn btn-primary">SRI PLANTING</a>
                    </div>
                    <div class="col-md-12 unactive-element widget-bg" id="pro_planting" >
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
                                <a href="<?php echo base_url('start_planting?mode=pro_planting&plants_id='.$plants_id);?>" class="btn btn-primary">PRO PLANTING</a>
                    </div>
                    <!-- /.widget-holder -->
                </div>
<!--
                	<div style="col-md-12 col-sm-12 widget-holder widget-full-height">
		<canvas id="canvas"></canvas>
	</div>
-->
	<br>
	<br>
                <!-- /.row -->
                <!-- Chart Group 2 -->
                <div class="row">
                    <!-- Charts: Tasks -->
                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                    </div>
                    <!-- /.widget-holder -->

            </div>
            <!-- /.widget-list -->
            </div>