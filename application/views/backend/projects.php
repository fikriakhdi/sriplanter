<!--open content add divice-->
<div class="pos-title">
    <h3>Select Projects</h3>
</div>
<div class="content-add-device" id="add_device">
<!--
    <div class="head-button-pos">
        <a href="<?php echo base_url('add_device');?>"><span class="fa fa-plus"></span>Add Device</a>
    </div>
-->

    <div class="pos-data-table table-responsive data-table-project-for-pc" id="project_data_table">
        <table class="table table-striped table-bordered datatable">
            <thead>
            <tr>
                <th>
                    <form action="" method="">
                        <input type="checkbox" name="vehicle1" value="Bike" checked>
                    </form>
                </th>
                <th>Project Name</th> 
                <th>Plant Name</th>
                <th>Avg Mois</th>
                <th>Avg Light</th>
                <th>Avg Temp</th>
                <th>Avg Hum</th>
                <th>Avg pH</th>
                <th>Date Start</th>
                <th>Harvest Estimation</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php 
                $projects = get_project_list("", $member->id);
                if($projects!=false){
                    foreach($projects->result() as $project){
                        $harvest_estimation = date('Y-m-d', strtotime($project->datecreated. ' + '.$project->harvest_time.' days'));
                ?>
            <tr class="des-table">
                <td>
                    <form action="" method="">
                        <input type="checkbox" name="vehicle1" value="Bike" checked>
                    </form>
                </td>
                <td><?php echo $project->name;?></td>
                <td><?php echo $project->plants_name;?></td>
                <td><?php echo (INT)$project->avg_mois;?></td>
                <td><?php echo (INT)$project->avg_light;?></td>
                <td><?php echo (INT)$project->avg_temp;?>C</td>
                <td><?php echo number_format((float)$project->avg_hum, 2, '.', '');?>%</td>
                <td><?php echo number_format((float)$project->avg_ph, 1, '.', '');?></td>
                <td><?php echo date('Y-m-d', strtotime($project->datecreated));?></td>
                <td><?php echo $harvest_estimation;?></td>
                <td>
                    <div class="pos-btn-collapse">
                        <div class="left">
                              <a class="" href="<?php echo base_url('project_detail/'.$project->id);?>"><i class="list-icon material-icons">details</i></a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i></a>
                          </div>
                    </div>
                </td>
            </tr>
                <?php } } ?>
                </tbody>
        </table>
        <div class="pos-btn-select">
            <button onclick="" class="button select-btn" type="button">Delete</button>
        </div>
    </div>
    <div class="data-table-project-for-mobile hidden-md hidden lg">
        <div class="pos-con-data-for-mobile-project">
            <div class="wrapper">
              <div class="half">
                <div class="tab blue">
                  <input id="tab-four" type="radio" name="tabs2">
                  <label for="tab-four">Project 1</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-five" type="radio" name="tabs2">
                  <label for="tab-five">Project 2</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-six" type="radio" name="tabs2">
                  <label for="tab-six">Project 3</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                  <div class="tab blue">
                  <input id="tab-four" type="radio" name="tabs2">
                  <label for="tab-four">Project 4</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-five" type="radio" name="tabs2">
                  <label for="tab-five">Project 5</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab blue">
                  <input id="tab-six" type="radio" name="tabs2">
                  <label for="tab-six">Project 6</label>
                  <div class="tab-content">
                    <table>
                        <tr class="one-1">
                            <td class="left">Project Name</td>
                            <td class="node">:</td>
                            <td class="right">Baby Tomat 1</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Project ID</td>
                            <td class="node">:</td>
                            <td class="right">SRI92212</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Plant Name</td>
                            <td class="node">:</td>
                            <td class="right">Tomato</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Mois</td>
                            <td class="node">:</td>
                            <td class="right">458</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Light</td>
                            <td class="node">:</td>
                            <td class="right">435230</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg Temp</td>
                            <td class="node">:</td>
                            <td class="right">28C</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Avg Hum</td>
                            <td class="node">:</td>
                            <td class="right">78%</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Avg pH</td>
                            <td class="node">:</td>
                            <td class="right">6,8</td>
                        </tr>
                        <tr class="one-1">
                            <td class="left">Date Start</td>
                            <td class="node">:</td>
                            <td class="right">01-01-2019</td>
                        </tr>
                        <tr class="two-2">
                            <td class="left">Harvest Estimation</td>
                            <td class="node">:</td>
                            <td class="right">30-02-2019</td>
                        </tr>
                    </table>
                      <div class="pos-btn-data-table-mobile-project">
                          <div class="left">
                              <a class="" href="<?php echo base_url('real_time_monitoring_project');?>"><i class="list-icon material-icons">details</i>Detail</a>
                          </div>
                          <div class="right">
                              <a class="" href="#"><i class="list-icon material-icons">edit</i>Edit</a>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!--close content add device-->
