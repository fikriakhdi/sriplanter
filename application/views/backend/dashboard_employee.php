           
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.css');?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.print.min.css');?>' rel='stylesheet' media='print' />
<script src='<?php echo base_url('assets/plugins/fullcalendar/lib/moment.min.js');?>'></script>
<script src='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.js?1');?>'></script>
<!-- <script src='<?php //echo base_url('assets/js/event_calendar.js');?>'></script> -->
<link href="<?php echo base_url('assets/css/event_calendar.css');?>" rel="stylesheet">
<!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h5 class="mr-0 mr-r-5">Dashboard</h5>
                    <p class="mr-0 text-muted d-none d-md-inline-block">statistics, charts, events and reports</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <!-- Counters -->
                <div class="row">
                  <div class="col-md-12">
                  </div>
                </div>
                <!-- /.row -->
                <!-- Chart Group 1 -->
                <div class="row">
                    <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                        <div class="widget-bg bg-secondary text-inverse">
                            <div class="widget-body clearfix">
                                <div class="widget-counter">
                                    <h6>Total Projects </h6>
                                    <h3 class="h1"><span class="counter">3</span></h3><i class="material-icons list-icon">folder</i>
                                </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- Chart: Registration History -->
                    <div class="col-md-3 col-sm-6 widget-holder widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body clearfix">
                                <div class="widget-counter">
                                    <h6>Total Task </h6>
                                    <h3 class="h1"><span class="counter">346</span></h3><i class="material-icons list-icon">event_available</i>
                                </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                        <div class="widget-bg bg-color-scheme text-inverse">
                            <div class="widget-body clearfix">
                                <div class="widget-counter">
                                    <h6>Total Income</h6>
                                    <h3 class="h1"><span class=""><?php echo money('14000000');?></span></h3><i class="material-icons list-icon">attach_money</i>
                                </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="col-md-6 widget-holder widget-full-height">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title">
                                <i class="material-icons list-icon">folder</i> My Projects </h5>
                                <br>
                                <div class="padded-reverse">
                                    <table class="table table-striped widget-status-table mr-b-0">
                                        <thead>
                                            <tr>
                                                <th class="pd-l-20">Name</th>
                                                <th>Status</th>
                                                <th class="hidden-xs">Deadline</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Tarcho Tee</a>
                                                </th>
                                                <td><span class="badge badge-info text-inverse">Complete</span>
                                                </td>
                                                <td class="text-muted hidden-xs">July 31,2017</td>
                                            </tr>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Athlete Tee</a>
                                                </th>
                                                <td><span class="badge badge-danger text-inverse">Pending</span>
                                                </td>
                                                <td class="text-muted hidden-xs">April 12, 2017</td>
                                            </tr>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Namche Zip Tee</a>
                                                </th>
                                                <td><span class="badge badge-warning text-inverse">On Progress</span>
                                                </td>
                                                <td class="text-muted hidden-xs">August 3, 2017</td>
                                            </tr>
                                        </tbody>
                                        <th><a href="<?php echo base_url('my_projects');?>"> <span class="fa fa-arrow-circle-right"></span> Show More</a></th>
                                    </table>
                                    
                                    <!-- /.widget-status-table -->
                                </div>
                                <!-- /.padded-reverse -->
                            </div>
                            <!-- /.widget-body badge -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="col-md-6 widget-holder widget-full-height">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title">
                                <i class="material-icons list-icon">folder_shared</i> My Documents </h5>
                                <br>
                                
                                <div class="padded-reverse">
                                    <table class="table table-striped widget-status-table mr-b-0">
                                        <thead>
                                            <tr>
                                                <th class="pd-l-20">Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Purchase Order</a>
                                                </th>
                                                <td><a href="" class="btn btn-primary">Download</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Athlete Tee</a>
                                                </th>
                                                <td><a href="" class="btn btn-primary">Download</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="pd-l-20"><a href="#">Namche Zip Tee</a>
                                                </th>
                                                <td><a href="" class="btn btn-primary">Download</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <th><a href="<?php echo base_url('my_projects');?>"> <span class="fa fa-arrow-circle-right"></span> Show More</a></th>
                                    </table>
                                    
                                    <!-- /.widget-status-table -->
                                </div>
                                <!-- /.padded-reverse -->
                            </div>
                            <!-- /.widget-body badge -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="col-md-6 widget-holder widget-full-height">
                        <div class="widget-bg bg-color-scheme  text-inverse">
                            <div class="widget-body clearfix">
                                <h5 class="box-title">
                                <i class="material-icons list-icon">date_range</i> Calendar </h5>
                                <br>
                                <div class="padded-reverse">
                                    <div id="calendar" style="padding:3%"></div>
                                    <!-- /.widget-status-table -->
                                </div>
                                <!-- /.padded-reverse -->
                            </div>
                            <!-- /.widget-body badge -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
                <!-- Chart Group 2 -->
                <div class="row">
                    <!-- Charts: Tasks -->
                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                    </div>
                    <!-- /.widget-holder -->

            </div>
            <!-- /.widget-list -->
            </div>
<script>
$(document).ready(function() {

		$('#calendar').fullCalendar({
			defaultDate: '2019-01-01',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
					{
						title: 'First day work',
						start: '2019-01-01',
						end: '2019-01-01',
						url: 'http://wsp.jualrumah99.com',
						disableDragging: true,
						editable: false
					}
			]
		});

	});

</script>