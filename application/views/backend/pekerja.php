<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-user"style="padding-right:10px"></span> Pekerja</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="pekerja_list_menu" href="#pekerja_list" class="active" data-toggle="tab">Pekerja List</a> </li>
         <li> <a id="add_pekerja_menu" href="#add_pekerja" data-toggle="tab"> Add Pekerja</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="pekerja_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Username</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Telp</th>
              <th>Gender</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_pekerja_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->username;?></td>
              <td><?php echo $data->nama_pekerja;?></td>
              <td><?php echo $data->alamat_pekerja;?></td>
              <td><?php echo $data->telp_pekerja;?></td>
              <td><?php echo strtoupper($data->gender_pekerja);?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/pekerja_edit/'.$data->id_pekerja);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_pekerja/'.$data->id_pekerja);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_pekerja">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_pekerja');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label>User<span style="color:#f00">*</span></label>
        <select class="form-control" name="id_user" id="id_user" required>
          <?php
          $data_list = get_all_member_list();
          if($data_list!=false){
            echo '<option>Pilih User</option>';
            foreach($data_list->result() as $data){
              echo '<option value="'.$data->id_user.'">'.$data->username.'</option>';
            }
          } else echo '<option>Maaf tidak ada pilihan user</option>';
          ?>
        </select>
      </div>
      <div class="form-group">
        <label>Nama<span style="color:#f00">*</span></label>
        <input type="text" class="form-control" name="nama_pekerja" placeholder="Ketik Nama" maxlength="150" required>
      </div>
      <div class="form-group">
        <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
        <input type="text" class="form-control" name="alamat_pekerja" placeholder="Ketik Alamat"  required>
      </div>
      <div  class="form-group">
        <label>Telp<span style="color:#f00">*</span></label>
        <input type="text" class="form-control" name="telp_pekerja" placeholder="Ketik No Telp" required>
      </div>
      <div  class="form-group">
        <label>Gender<span style="color:#f00">*</span></label>
        <select class="form-control" name="gender_pekerja"  value="<?php echo $data_edit->stok_produk;?>" required>
          <option>Pilih Gender</option>
          <option value="pria">Pria</option>
          <option value="wanita">Wanita</option>
        </select>
      </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
