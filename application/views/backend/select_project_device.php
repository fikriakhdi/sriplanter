<!--open content add divice-->
<div class="pos-title">
    <h3>Select Project</h3>
</div>
<div class="content-add-device" id="add_device">

    <div class="pos-data-table" id="project_data_table">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Project Name</th> 
                <th>Project ID</th>
                <th>Plane Name</th>
                <th>Avg Mois</th>
                <th>Avg Light</th>
                <th>Avg Temp</th>
                <th>Avg Hum</th>
                <th>Avg pH</th>
                <th>Date Start</th>
                <th>Harvest Estimation</th>
                <th>Action</th>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
            <tr class="des-table">
                <td>Baby Tomat 1</td>
                <td>SRI92212</td>
                <td>Tomato</td>
                <td>458</td>
                <td>435230</td>
                <td>28C</td>
                <td>78%</td>
                <td>6,8</td>
                <td>01-01-2019</td>
                <td>30-02-2019</td>
                <td>
                    <a href="#ProcessSychronize" role="button" class="btn-orange-suctom-datatble" data-toggle="modal">synchronize</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<!--close content add device-->

<div class="modal fade" id="ProcessSychronize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clock-o"></i> Please Wait</h4>
      </div>
      <div class="modal-body center-block">
        <p>Sychronize Process</p>
        <div class="progress">
          <div class="progress-bar bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $('#ProcessSychronize').on('shown.bs.modal', function () {
 
    var progress = setInterval(function() {
    var $bar = $('.bar');

    if ($bar.width()==500) {
      
        // complete
      
        clearInterval(progress);
        $('#ProcessSychronize').removeClass('active');
        $('#ProcessSychronize').modal('toggle');
        $bar.width(0);
        
    } else {
      
        // perform processing logic here
      
        $bar.width($bar.width()+50);
    }
    
    $bar.text($bar.width()/5 + "%");
	}, 800);
  
  
})
</script>