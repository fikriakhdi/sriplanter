<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-list" style="padding-right:10px"></span> Order Barang</h2>
            <hr>
        </div>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_order_barang');?>" enctype="multipart/form-data">
                            <div class="form-group">
                              <label for="nama_barang">Nama Barang<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="nama_barang" name="nama_barang" aria-describedby="emailHelp" placeholder="Type Nama Barang" maxlength="100"  value="<?php echo $data_edit->nama_barang;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="jumlah_barang">Jumlah Barang<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="jumlah_barang" name="jumlah_barang" aria-describedby="emailHelp" placeholder="Type Jumlah Barang" maxlength="11"  value="<?php echo $data_edit->jumlah_barang;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">Alamat Pengirim<span style="color:#f00">*</span></label>
                              <textarea type="text" class="form-control" id="alamat_pengirim" name="alamat_pengirim" aria-describedby="emailHelp" placeholder="Type Alamat Pengirim" value="<?php echo $data_edit->alamat_pengirim;?>"  required></textarea>
                            </div>
                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
</section>
