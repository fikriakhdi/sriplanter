
<link href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css');?>" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
          <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                  <h5 class="mr-0 mr-r-5">Pengeluaran</h5>
                  <p class="mr-0 text-muted d-none d-md-inline-block">Mengatur semua data pengeluaran</p>
              </div>
          </div>
          <!-- /.page-title -->
          <!-- =================================== -->
          <!-- Different data widgets ============ -->
          <!-- =================================== -->
          <div class="widget-list">
              <div class="row">
                <div class="col-md-12">
                  <?php
                  echo (!empty($this->session->flashdata('message'))?$this->session->flashdata('message'):'');
                   ?>
                 </div>
                 <div class="col-md-12" id="top_button">
                   <a class="btn btn-primary btn-rounded" href="<?php echo base_url('create_expenses');?>">Buat Pengeluaran</a>
                 </div>
                 <div class="col-md-12 widget-holder widget-full-height">
                   <div class="widget-bg ">
                       <div class="widget-heading clearfix">
                           <h5>List Pengeluaran</h5>
                           <!-- /.widget-actions -->
                       </div>
                       <!-- /.widget-heading -->
                       <div class="widget-body clearfix ">
                           <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper">
                           <table class="table table-hover">
                             <thead>
                               <tr>
                                 <th>NO</th>
                                  <th>Nominal</th>
                                  <th>Kategori</th>
                                  <th>Tanggal</th>
                                  <th>Deskripsi</th> 
                                  <th>Tipe</th>
                                  <th>Bukti Pembayaran</th>
                                  <th>Ubah</th>
                                  <th>Hapus</th>
                               </tr>
                             </thead>
                             <tbody>
                               <?php
                                  $data_list = get_all_expenses_list();
                                  if($data_list!=false){
                                      $num=0;
                                      foreach($data_list->result() as $data){
                                          $num++;
                                          $receipt = ($data->picture!="empty"?"<a href='".base_url($data->picture)."' class='btn btn-primary' target='_blank'>Lihat</a>":'Tidak ada');
                                          ?>
                                <tr>
                                  <td><?php echo $num;?></td>
                                  <td><?php echo money($data->nominal);?></td>
                                  <td><?php echo strtoupper($data->category);?></td>
                                  <td><?php echo $data->date;?></td>
                                  <td><?php echo $data->description;?></td>
                                  <td><?php echo strtoupper($data->type);?></td>
                                  <td><?php echo $receipt;?></td>
                                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('expenses_edit/'.$data->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="fa fa-edit"></span></a></p></td>
                                    <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete-confirm" data-toggle="modal" data-target="#delete_modal" data-url="<?php echo base_url('expenses_delete/'.$data->id);?>" id_data="<?php echo $data->id;?>" data-toggle="modal" data-target="#delete_modal"><span class="fa fa-trash"></span></button></p></td>
                                </tr>
                                  <?php }} ?>
                             </tbody>
                           </table>
                               </div>
                       </div>
                       <!-- /.widget-body -->
                   </div>
                 </div>

        </div>
      </div>
    <!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>






