<!DOCTYPE html>
<html lang="en">

<head>
    <script src="<?php echo base_url('assets/plugins/pace/pace.min.js');?>"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/favicon.png');?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title;?></title>
    <!-- CSS -->
    <link href="<?php echo base_url('assets/vendors/material-icons/material-icons.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/responsive_sriplant.css?'.uniqid());?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendors/mono-social-icons/monosocialiconsfont.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/sweetalert/sweetalert2.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/magnific_popup/magnific-popup.min.css');?>" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/oscar_style.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/custom-style-wsp.css');?>" rel="stylesheet" type="text/css">
    
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body id="pos-log-page" class="body-bg-full profile-page" style="background-image: url('<?php echo base_url('assets/img/background.jpg');?>')">
    <div id="wrapper" class="row wrapper">
        <div class="col-md-8 text-inverse d-none d-sm-block">
            <div class="login-left-inner">
                <div class="login-content">
                    <img alt="" src="<?php echo base_url('assets/img/sriplanter-white.png');?>" style="width: 50%!important;">
                    <p><small></small>
                    </p>
                </div>
            </div>
            <!-- /.login-left-inner -->
        </div>
        <!-- /.login-left -->
        <div class="col-10 ml-auto ml-sm-auto col-sm-8 col-md-4 login-right float-right" id="custom-pos-login-mobile">
            <div class="navbar-header navbar-header-custom text-center">
                <a href="<?php echo base_url();?>">
                    <img alt="" src="<?php echo base_url('assets/img/logo-sri.png');?>">
                </a>
            </div>
            <br>
            <!-- /.navbar-header -->
            
            
            <span id="warning_message"></span>
    <form action="<?php echo base_url('backend/login_process');?>" class="form-material" method="post" id="login-form" refresh="<?php echo base_url('');?>">
                <div class="form-group form-group-custom">
                    <input type="text" placeholder="johndoe@site.com" class="form-control form-control-line form-control-custom" name="username" id="username">
                    <label for="example-email">Username</label>
                </div>
                <div class="form-group form-group-custom">
                    <input type="password" placeholder="password" class="form-control form-control-line form-control-custom" name="password" id="password">
                    <label>Password</label>
                </div>
                <input type="hidden" name="redirect" id="redirect" value="<?php if(!empty($redirect)) echo $redirect;?>">
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-color-scheme ripple submit-btn btn-lg-custom" type="submit" form-id="login-form">Sign In</button>
                </div>
                <div class="form-group no-gutters mb-0">
                    <div class="col-md-12 d-flex">
<!--
                        <div class="checkbox checkbox-info mr-auto">
                            <label class="d-flex">
                                <input type="checkbox"> <span class="label-text">Remember me</span>
                            </label>
                        </div>
-->
                        <a href="<?php echo base_url('forgot_password');?>" id="to-recover" class="my-auto pb-2 text-right text-right-custom"><i class="fa fa-lock mr-1"></i>Forgot Password?</a>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.form-group -->
            </form>
            <!-- /.form-material -->
        </div>
        <!-- /.login-right -->
    </div>
    <!-- /.body-container -->
    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/material-design.js');?>"></script>
    <script src="<?php echo base_url('assets/js/submit.js');?>"></script>
</body>

</html>