<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-file"style="padding-right:10px"></span> Nota</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
        <div class="content-datatable table-responsive">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr class="title-datable">
                <th>NO</th>
                <th>Nama Pelanggan</th>
                <th>Total Keseluruhan</th>
                <th>Tanggal Penjualan</th>
                <th>Cetak</th>
              </tr>
            </thead>
            <tbody>
                <?php
                $data_list = get_all_nota_list();
                if($data_list!=false){
                    $num=0;
                    foreach($data_list->result() as $data){
                        $num++;
                        ?>
              <tr>
                <td><?php echo $num;?></td>
                <td><?php echo $data->nama_pelanggan;?></td>
                <td><?php echo money($data->total_keseluruhan);?></td>
                <td><?php echo $data->tgl_penjualan;?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Cetak"><a href="<?php echo base_url('backend/cetak_nota/'.$data->id_penjualan);?>" class="btn btn-warning btn-xs" data-title="Print"  ><span class="fa fa-print"></span></a></p></td></tr>
                <?php }} ?>
            </tbody>
          </table>
        </div>

    </div>
  </div>
</div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
