<!--open content tips and trick-->
<div class="pos-title">
    <h3>Tips & Trick</h3>
</div>
<div class="content-tips-trick" id="tips-trick">
    <div class="pos-con-tisp">
        <?php 
            $article_content= get_all_article();
            if ($article_content !=false) {
                foreach ($article_content->result() as $data_article_content){
            ?>
        <div class="col-pos-tips" style="background-image:url(<?php echo base_url($data_article_content->image_article);?>);background-size:cover">
            <div class="content-pos-tips">
                  <div class="overlay">
                    <div class="text">
                        <p><?php echo $data_article_content->description_article;?></p>
                        <a class="btn-see-more" href="<?php echo base_url('article');?>">See More</a>
                    </div>
                  </div>
            </div>
        </div>
            <?php }} else echo 'Sorry, There\'s no plants to choose.' ?>
    </div>
</div>