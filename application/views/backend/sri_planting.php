<!--open content start palanting-->
<div class="pos-title">
    <h3>DETAIL PROJECT</h3>
</div>
<div class="content-start-planting" id="start-palnting">
    <div class="col-md-12">
        <?php
            $message = $this->session->userdata('message');
            if(!empty($message)) echo $message;
        ?>
        </div>
    <a class="btn btn-primary btn-md" href="<?php echo base_url('select_mode/'.$plants_id);?>"><span class="fas fa-arrow-left"></span> Back</a><br><br>
    <div class="pos-start-planting-form">
        <form class="" method="post" action="<?php echo base_url('backend/start_planting_process');?>" id="form-start-planting">
            <div class="form-group">
                <label>Project Name :</label>
                <input type="text" class="form-control" placeholder="Type Your Project Name" name="project_name" id="project_name">
            </div>
            <div class="form-group">
                <label>Date Start :</label>
                <input type="text" class="form-control date_picker harvest_estimation" placeholder="Choose Your Date Start" name="date_start" id="date_start" url="<?php echo base_url('backend/harvest_estimation');?>" harvest-time="<?php echo $plants->harvest_time;?>">
            </div>
            <div class="form-group">
                <label>Harvest Estimation :</label>
                <input type="text" class="form-control" placeholder="Harvest Estimation" name="harverst_estimation" id="harverst_estimation" readonly>
            </div>
            <div class="form-group">
                <label>Plants name :</label>
                <input type="text" class="form-control" name="plant_name" id="plant_name" value="<?php echo $plants->name.' ('.$plants->latin_name.')';?>" readonly>
            </div>
            <div class="pos-add-more-device">
                <div class="title-secod">
                    <h3 class="secon-title-form">Syncronize Your Device</h3> 
                </div>
                <div class="col-md-12 right-content">
                <button class="btn btn-primary btn-md pull-right add-more-device" type="button"><span class="fas fa-plus"></span> Add More Device</button>
                </div>
                <div class="pos-col-add-device-more" id="synchronize_device">
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_1">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="1" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="1" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_1">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="1" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_1">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_2">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="2" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="2" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_2">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="2" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_2">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_3">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="3" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="3" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_3">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="3" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_3">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_4">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="4" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="4" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_4">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="4" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_4">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_5">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="5" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="5" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_5">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="5" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_5">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_6">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="6" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="6" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_6">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="6" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_6">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_7">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="7" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="7" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_7">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="7" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_7">
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more hidden" id="added_device_8">
                            <img class="image" src="<?php echo base_url($plants->picture);?>">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="8" >close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#EditDevice" data-sequence="8" class="edit-device"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="pos-cont-col-add-device-more dashed-border" id="empty_device_8">
                            <button type="button" class="btn-plus-add-device save-device" data-sequence="8" data-toggle="modal" data-target="#AddDevice">+</button>
                        </div>
                        <input type="hidden" name="devices[]" class="devices" id="value_device_8">
                    </div>
<!--                    hidden input-->
                    <input type="hidden" id="total_slot" value="8">
                    <input type="hidden" name="plants_id" id="plants_id" value="<?php echo $plants->id;?>">
                    <input type="hidden" name="mode" id="mode" value="sri_planting">
                    <input type="hidden" id="picture_url" value="<?php echo base_url($plants->picture);?>">
                </div>
            </div>
            <div class="pos-btn-submit-start-planting">
                <button type="submit" class="btn btn-success">Start Now</button>
            </div>
        </form>
    </div>
</div>
<!--close content start palanting-->

<!--open modal input device id-->
  <div class="modal fade" id="AddDevice" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Device</h4>
        </div>
        <div class="modal-body">
          <form>
              <div class="form-group form-group-custom">
                  <input type="text" class="form-control form-control-custom capitalize" id="device_id" placeholder="Input Device Code" name="device_id">
                  <input type="hidden" id="sequence_data" value="">
              </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn-save-custom save-device-id" data-url="<?php echo base_url('backend/device_code_check');?>">SAVE DEVICE</button>
            <button type="button" class="btn-cancle-custom " data-dismiss="modal" id="cancel_modal_device">CANCEL</button>
        </div>
      </div>
      
    </div>
  </div>
<!--close modal input device id-->

<!--open modal input device id-->
  <div class="modal fade" id="EditDevice" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Device</h4>
        </div>
        <div class="modal-body">
          <form>
              <div class="form-group form-group-custom">
                  <input type="text" class="form-control form-control-custom capitalize" id="device_id_edit" placeholder="Input Device Code" name="device_id">
                  <input type="hidden" id="sequence_data_edit" value="">
              </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn-save-custom edit-device-id" data-url="<?php echo base_url('backend/device_code_check');?>">SAVE CHANGE</button>
            <button type="button" class="btn-cancle-custom " data-dismiss="modal" id="cancel_edit_modal_device">CANCEL</button>
        </div>
      </div>
      
    </div>
  </div>
<!--close modal input device id-->