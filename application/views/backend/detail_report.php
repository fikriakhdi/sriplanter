<!--open content detile project-->
<div class="pos-title">
    <h3>TOMAT 1</h3>
</div>
<div class="content-detile-project" id="detile-project">
    <div class="pos-btn-right">
        <a href="<?php echo base_url('view_all_chart');?>" class="open-all-report">Open All Report</a>
    </div>
    <div class="pos-cont-detile-pro">
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('water_chart');?>">
                <div class="pos-col-detile-pro col-detile-pro-blue">
                    <i class="fas fa-water"></i>
                    <div class="head">
                        <h4>Water Usage</h4>
                    </div>
                    <div class="body">
                        <h3>124</h3><span>litre</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('farming_days_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-orange">
                <i class="fas fa-calendar-alt"></i>
                <div class="head">
                    <h4>Farming Days</h4>
                </div>
                <div class="body">
                    <h3>35</h3><span>Days</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('soil_moisture_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-pink">
                <i class="fas fa-tint"></i>
                <div class="head">
                    <h4>soil moisture</h4>
                </div>
                <div class="body">
                    <h3>550</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('soil_ph_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue-sky">
                <i class="fas fa-thermometer"></i>
                <div class="head">
                    <h4>soil ph</h4>
                </div>
                <div class="body">
                    <h3>6,4</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('light_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue">
                <i class="fas fa-sun"></i>
                <div class="head">
                    <h4>light</h4>
                </div>
                <div class="body">
                    <h3>14.359</h3>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('temprature_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-orange">
                <i class="fas fa-thermometer-three-quarters"></i>
                <div class="head">
                    <h4>temperature</h4>
                </div>
                <div class="body">
                    <h3>25</h3>
                    <strong>&#176;</strong><span>C</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('humidity_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-pink">
                <i class="fas fa-wind"></i>
                <div class="head">
                    <h4>humidity</h4>
                </div>
                <div class="body">
                    <h3>68</h3>
                    <span>%</span>
                </div>
            </div>
            </a>
        </div>
        <div class="col-md-3 col-detile-pro ">
            <a href="<?php echo base_url('harvest_estimation_chart');?>">
            <div class="pos-col-detile-pro col-detile-pro-blue-sky">
                <i class="fab fa-pagelines"></i>
                <div class="head">
                    <h4>harvest estimation</h4>
                </div>
                <div class="body">
                    <h3>32</h3>
                    <span>day left</span>
                </div>
            </div>
            </a>
        </div>
    </div>
</div>
<!--close content detile project-->