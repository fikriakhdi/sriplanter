
<link href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css');?>" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
          <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                  <h5 class="mr-0 mr-r-5">Pekerja</h5>
                  <p class="mr-0 text-muted d-none d-md-inline-block">Mengatur semua data pekerja</p>
              </div>
          </div>
          <!-- /.page-title -->
          <!-- =================================== -->
          <!-- Different data widgets ============ -->
          <!-- =================================== -->
          <div class="widget-list">
              <div class="row">
                <div class="col-md-12">
                  <?php
                  echo (!empty($this->session->flashdata('message'))?$this->session->flashdata('message'):'');
                   ?>
                 </div>
                 <div class="col-md-12" id="top_button">
                   <a class="btn btn-primary btn-rounded" href="<?php echo base_url('employee');?>"><span class="fa fa-arrow-left"></span> Kembali</a>
                 </div>
                 <div class="col-md-12 widget-holder widget-full-height">
                   <div class="widget-bg">
                       <div class="widget-heading clearfix">
                           <h5>Buat Pekerja</h5>
                           <!-- /.widget-actions -->
                       </div>
                       <!-- /.widget-heading -->
                       <div class="widget-body clearfix">
                          <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_employee');?>" enctype="multipart/form-data">
                              <input name="id" type="hidden" value="">
                              <div class="form-group">
                                <label>Nama<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control" name="name" placeholder="Ketik Nama" maxlength="150"  value="" required>
                              </div>
                              <div class="form-group">
                                <label>Email<span style="color:#f00">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Ketik Email" maxlength="150"  value="" required>
                              </div>
                              <div class="form-group">
                                <label>Password<span style="color:#f00">*</span></label>
                                <input type="password" class="form-control" name="password" placeholder="Ketik Password" maxlength="150"  value="" required>
                              </div>

                              <div class="form-group">
                                <label>Foto Profile<span style="color:#f00">*</span></label>
                              <div class="venue-pictures">
                                  <img id="file" class="img-thumbnail img-responsive change_picture" data-file="profile_picture" src="<?php echo base_url('assets/images/no-image.jpg');?>">
                                  <input type="file" class="form-control file_input_logo" name="profile_picture" id="profile_picture" onchange="imagepreview(this, 'file')">
                                </div>
                              </div>
                              <div class="form-group">
                                <label>Tanggal Lahir<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control date_picker" name="date_of_birth" placeholder="Pilih Tanggal Lahir" maxlength="150"  value="" required>
                              </div>
                              <div class="form-group">
                                <label>Alamat<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control" name="address" placeholder="Ketik Alamat"  value="" required>
                              </div>
                              <div class="form-group">
                                <label>No. KTP<span style="color:#f00">*</span></label>
                                <input type="number" class="form-control" name="id_number" placeholder="Ketik No KTP"  value="" required>
                              </div>
                              <div class="footer-form">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                          </form>

                       </div>
                       <!-- /.widget-body -->
                   </div>
                 </div>

        </div>
      </div>
    <!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
