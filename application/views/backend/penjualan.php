<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-usd"style="padding-right:10px"></span> Penjualan</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="Penjualan_list_menu" href="#penjualan_list" class="active" data-toggle="tab">Penjualan List</a> </li>
         <li> <a id="add_Penjualan_menu" href="#add_penjualan" data-toggle="tab"> Add Penjualan</a> </li>
         <li> <a id="add_pelanggan_menu" href="#add_pelanggan" data-toggle="tab"> Add Pelanggan</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="penjualan_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Pelanggan</th>
              <th>Total Penjualan</th>
              <th>Diskon</th>
              <th>Total Keseluruhan</th>
              <th>Status Pembayaran</th>
              <th>Tanggal Penjualan</th>
              <th>Detail</th>
<!--              <th>Delete</th>-->
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_penjualan_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_pelanggan;?></td>
              <td><?php echo money($data->total_penjualan);?></td>
              <td><?php echo money($data->diskon);?></td>
              <td><?php echo money($data->total_keseluruhan);?></td>
              <td><?php echo strtoupper($data->status_pembayaran);?></td>
              <td><?php echo $data->tgl_penjualan;?></td>
              <td><p data-placement="top" data-toggle="tooltip" title="Detail"><a href="<?php echo base_url('backend/penjualan_detail/'.$data->id_penjualan);?>" class="btn btn-warning btn-xs" data-title="Detail"  ><span class="fa fa-search-plus"></span></a></p></td>
<!--              <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_penjualan/'.$data->id_penjualan);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>-->
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_penjualan">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_penjualan');?>" enctype="multipart/form-data">
          <input name="id" type="hidden" value="">
          <div class="form-group">
            <label for="nama_Penjualan">Pelanggan<span style="color:#f00">*</span></label>
            <select name="id_pelanggan" class="form-control" required>
              <?php
              $pelanggan_list = get_all_pelanggan_list();
              if($pelanggan_list!=false){
                echo '<option value="">Pilih Pelanggan</option>';
                foreach($pelanggan_list->result() as $pelanggan){
                  echo '<option value="'.$pelanggan->id_pelanggan.'">'.$pelanggan->nama_pelanggan.'</option>';
                }
              } else echo '<option>Tidak ada pilihan pelanggan</option>';
              ?>
            </select>
          </div>
          <div class='form-group'>
            <label>List Produk</label>
            <table class="table">
              <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Stok</th>
                <th>Harga</th>
                <th>Harga Jual</th>
                <th>Qty</th>
              </thead>
              <tbody>
                <?php
                $data_list = get_all_produk_list();
                if($data_list!=false){
                  $no = 0;
                  foreach($data_list->result() as $data){
                    $no++;
                    echo '<tr>';
                    echo '<td>'.$no.'<input type="hidden" name="id_'.$no.'" value="'.$data->id_produk.'"></td>';
                    echo '<td>'.$data->nama_produk.'</td>';
                    echo '<td>'.$data->jenis_produk.'</td>';
                    echo '<td>'.$data->stok_produk.'<input type="hidden" name="stok_'.$no.'" value="'.$data->stok_produk.'"></td>';
                    echo '<td>'.money($data->harga_produk).'<input type="hidden" name="price_'.$no.'" value="'.$data->harga_produk.'"></td>';
                    echo '<td><input class="form-control" type="number" name="harga_jual_'.$no.'"></td>';
                    echo '<td><input class="form-control" type="number" name="qty_'.$no.'"></td>';
                    echo '</tr>';
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <input type="hidden" name="jumlah_product" value="<?php echo $data_list->num_rows();?>">
          <div class="form-group">
            <label for="diskon">Diskon<span style="color:#f00">*</span></label>
            <input type="number" class="form-control" id="diskon" name="diskon" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="tanggal_penjualan">Tanggal Penjualan<span style="color:#f00">*</span></label>
            <input type="text" class="form-control datepicker" id="tgl_penjualan" name="tgl_penjualan" autocomplete="off" required>
          </div>
          <div class="form-group">
          <label for="status_pembayaran">Status Pembayaran<span style="color:#f00">*</span></label>
          <select class="form-control" name="status_pembayaran" id="status_pembayaran" required>
            <option value="lunas">Lunas</option>
            <option value="hutang">Hutang</option>
          </select>
            </div>
          <div class="form-group">
          <label for="surat_jalan">Surat Jalan<span style="color:#f00">*</span></label>
          <select class="form-control" name="surat_jalan" id="surat_jalan" required>
            <option value="ya">Ya</option>
            <option value="tidak">Tidak</option>
          </select>
            </div>
          <div class="footer-form">
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
    <div class="tab-pane fade in" id="add_pelanggan">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_pelanggan');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label>Nama<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="title" name="nama_pelanggan" aria-describedby="emailHelp" placeholder="Ketik name" maxlength="150"  value="" required>
        </div>
        <div class="form-group">
          <label for="no_telepon">Alamat<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="alamat" name="alamat_pelanggan" aria-describedby="emailHelp" placeholder="Ketik Alamat" maxlength="150"  value="" required>
        </div>
        <div>
          <label for="telp">No. Telepon<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="telp" name="telp_pelanggan" aria-describedby="emailHelp" placeholder="Ketik No Telepon" maxlength="13"  value="" required>
        </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
