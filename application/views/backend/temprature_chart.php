<!--open cntentn TEMPRATURE chart-->
<div class="pos-title">
    <h3>TEMPRATURE - TOMATO 1</h3>
</div>
<div class="content-temprature" id="temprature">
    <div class="pos-head-button-chart">
        <div class="left">
            <div class="form-group">
                <select name="carlist" form="carform">
                  <option value="Weekly1">Weekly 1</option>
                  <option value="Weekly2">Weekly 2</option>
                  <option value="Weekly3">Weekly 3</option>
                  <option value="Weekly4">Weekly 4</option>
                <option value="Weekly5">Weekly 5</option>
                </select>
            </div>
        </div>
        <div class="right">
            <button class="open-all-report" onclick="">EXPORT TO CSV</button>
        </div>
    </div>
    <div class="pos-body-chart">
        <div class="content-chart" id="temprature_chart"></div>
    </div>
</div>
<!--close cntentn water chart-->