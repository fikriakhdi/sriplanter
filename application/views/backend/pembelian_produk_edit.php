<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pembelian Produk</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('pembelian_produk');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/pembelian_produk_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_pembelian_produk;?>">
                            <div class="form-group">
                              <label for="nama_produk">Name produk<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Type Nama produk" maxlength="100"  value="<?php echo $data_edit->nama_produk;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="jenis_produk">Jenis produk<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="jenis_produk" name="jenis_produk" placeholder="Type Jenis produk" maxlength="100"  value="<?php echo $data_edit->jenis_produk;?>" required>
                            </div>
                            <div class="form-group">
                                <label for="satuan_produk">Satuan Barang<span style="color:#f00">*</span></label>
                                <input type="text" class="form-control" id="satuan_produk" name="satuan_produk" placeholder="Type Satuan Barang" maxlength="100"   value="<?php echo $data_edit->satuan_produk;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="harga_produk">Harga produk<span style="color:#f00">*</span></label>
                              <input type="number" class="form-control" id="harga_produk" name="harga_produk" placeholder="Type Harga produk" value="<?php echo $data_edit->harga_produk;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="min_stock">Min Stok<span style="color:#f00">*</span></label>
                              <input type="number" class="form-control" id="min_stok" name="min_stok" placeholder="Type Minimal Stock" value="<?php echo $data_edit->min_stok;?>" required>
                            </div>
                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
