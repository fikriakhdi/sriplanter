
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/pace.css');?>">
    <script src="<?php echo base_url('assets/plugins/pace/pace.min.js');?>"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/img/favicon.png');?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title;?></title>
    <!-- CSS -->
    <link href="<?php echo base_url('assets/css/eo.css?'.uniqid());?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/responsive_sriplant.css?'.uniqid());?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendors/material-icons/material-icons.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendors/mono-social-icons/monosocialiconsfont.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/sweetalert/sweetalert2.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/magnific_popup/magnific-popup.min.css');?>" rel="stylesheet" type="text/css">
    <!--css mediaelementplayer-->
    <link href="<?php echo base_url('assets/css/mediaelementplayer.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">-->
    <!--css perfect-scrollbar-->
    <link href="<?php echo base_url('assets/css/perfect-scrollbar.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendors/weather-icons-master/weather-icons.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/vendors/weather-icons-master/weather-icons-wind.min.css');?>" rel="stylesheet" type="text/css">
    <!--css daterangepicker -->
    <link href="<?php echo base_url('assets/css/daterangepicker.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.css" rel="stylesheet" type="text/css">-->
    <!--css morris-->
    <link href="<?php echo base_url('assets/css/morris.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">-->
    <!--css slick-->
    <link href="<?php echo base_url('assets/css/slick.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" type="text/css">-->
    <!--css slick-theme-->
    <link href="<?php echo base_url('assets/css/slick-theme.css');?>" rel="stylesheet" type="text/css">
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" type="text/css">-->
    <link href="<?php echo base_url('assets/css/oscar_style.css?'.uniqid());?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/custom.css?'.uniqid());?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/plugins/bootstrap_datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css">
    <!--css jquery.dataTables-->
    <link href="<?php echo base_url('assets/css/dataTables1.css');?>" rel="stylesheet" type="text/css">
<!--     <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"> -->
    <!-- Head Libs -->
    <!--JS modernizr-->
    <script src="<?php echo base_url('assets/js/modernizr.js');?>"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>-->
    <!--JS jquery-->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
    <!--JS popper-->
    <script src="<?php echo base_url('assets/js/popper.js');?>"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.js"></script>-->
    <script src="<?php echo base_url('assets/plugins/clockpicker/bootstrap-clockpicker.min.js');?>"></script>
    <link href="<?php echo base_url('assets/plugins/clockpicker/bootstrap-clockpicker.min.css');?>" rel="stylesheet" type="text/css">
    <!-- include summernote -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/summernote/summernote-bs4.css');?>">
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/summernote/summernote-bs4.js');?>"></script>
    <!--JS Chart-->
    <script src="<?php echo base_url('assets/js/Chart.js');?>"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>-->
</head>
<body class="header-light sidebar-dark sidebar-expand">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header">
                <a href="<?php echo base_url();?>" class="navbar-brand">
                    <img class="logo-expand" alt="" src="<?php echo base_url('assets/img/sriplanter-white.png');?>" style="width:54%;">
                    <img class="logo-collapse" alt="" src="<?php echo base_url('assets/img/sri-short-white.png');?>" style="width:75%;">
                    <!-- <p>OSCAR</p> -->
                </a>
            </div>
            <!-- /.navbar-header -->
            <!-- Left Menu & Sidebar Toggle -->
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i class="material-icons list-icon">menu</i></a>
                </li>
            </ul>
            <!-- /.navbar-left -->
            <!-- Search Form -->
            <!-- <form class="navbar-search d-none d-sm-block" role="search"><i class="material-icons list-icon">search</i>
                <input type="text" class="search-query" placeholder="Search anything..."> <a href="javascript:void(0);" class="remove-focus"><i class="material-icons">clear</i></a>
            </form> -->
            <!-- /.navbar-search -->
            <div class="spacer"></div>
            <!-- Button: Create New -->
            <!-- <div class="btn-list dropdown d-none d-md-flex"><a href="javascript:void(0);" class="btn btn-primary dropdown-toggle ripple" data-toggle="dropdown"><i class="material-icons list-icon fs-24">playlist_add</i> Create New</a>
                <div class="dropdown-menu dropdown-left animated flipInY"><span class="dropdown-header">Create new ...</span>  <a class="dropdown-item" href="#">Projects</a>  <a class="dropdown-item" href="#">User Profile</a>  <a class="dropdown-item" href="#"><span class="badge badge-pill badge-primary float-right">7</span> To-do Item</a>
                    <a
                    class="dropdown-item" href="#"><span class="badge badge-pill badge-color-scheme float-right">23</span> Mail</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="#"><i class="material-icons list-icon icon-muted pull-right">settings</i> <strong>Settings</strong></a>
                </div>
            </div> -->
            <!-- /.btn-list -->
            <!-- User Image with Dropdown -->
            <ul class="nav navbar-nav">
                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown"><span class="avatar thumb-sm"><img src="<?php echo base_url($member->profile_picture);?>" class="rounded-circle" alt=""> <i class="material-icons list-icon">expand_more</i></span></a>
                    <div
                    class="dropdown-menu dropdown-left dropdown-card dropdown-card-wide dropdown-card-dark text-inverse">
                        <div class="card">
                            <header class="card-heading-extra">
                                <div class="row">
                                    <div class="col-8">
                                        <h3 class="mr-b-10 sub-heading-font-family fw-300"><?php echo $member->name;?></h3><span class="user--online">Available <i class="material-icons list-icon">expand_more</i></span>
                                    </div>
                                    <div class="col-4 d-flex justify-content-end"><a href="<?php echo base_url('logout');?>" class="mr-t-10"><i class="material-icons list-icon">power_settings_new</i> Logout</a>
                                    </div>
                                    <!-- /.col-4 -->
                                </div>
                                <!-- /.row -->
                            </header>
                            <section class="card-header text-inverse">New notifications <span class="badge badge-border badge-border-inverted bg-danger mr-l-10">4</span>
                            </section>

                        </div>
    </div>
    </li>
    </ul>
    <!-- /.navbar-right -->
    </nav>
        
        <div id="delete_modal" class="modal fade bs-modal-lg show" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h5 class="modal-title" id="myLargeModalLabel">Delete Data</h5>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to delete this record?
            </p>
                                            </div>
                                            <div class="modal-footer"><a href="" id="delete_id" class="btn btn-danger btn-rounded">YES</a>
                                                <button type="button" class="btn btn-primary btn-rounded ripple text-left close-modal" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
        <div class="modal-backdrop fade show" id="fade_modal" style="display:none"></div>
    <!-- /.navbar -->
<link href="<?php echo base_url('assets/plugins/jqueryui/jquery-ui.min.css');?>" rel="stylesheet">
    <div class="content-wrapper">
      <?php
      $this->load->view('backend/sidebar');
      ?>
        <main class="main-wrapper clearfix">
            <?php $this->load->view($content);?>
        </main>
        <!-- /.main-wrappper -->

    </div>
    <!-- /.content-wrapper --> 
<script>
$(document).ready(function(){
  $('.datepickerrange').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {format: 'YYYY-MM-DD'}
  });
});
</script>
</div>
<!--/ #wrapper --> 
<!-- Scripts -->
    <!-- JS wow -->
    <script src="<?php echo base_url('assets/js/wow.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>-->
    <!-- JS jquery.stellar-->
<script src="<?php echo base_url('assets/js/jquery.stellar.js');?>"></script>
    <!-- JS popper version 2-->
    <script src="<?php echo base_url('assets/js/popper2.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>-->
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- JS magnific-popup-->
    <script src="<?php echo base_url('assets/js/magnific-popup.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>-->
    <!-- JS mediaelementplayer-->
    <script src="<?php echo base_url('assets/js/mediaelementplayer.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>-->
    <!-- JS metisMenu-->
    <script src="<?php echo base_url('assets/js/metismenu.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>-->
    <!-- JS perfect-scrollbar-->
    <script src="<?php echo base_url('assets/js/perfect-scrollbar.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>-->
    <!-- JS sweetalert2-->
    <script src="<?php echo base_url('assets/js/sweetalert2.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>-->
    <!-- JS jquery.counterup-->
    <script src="<?php echo base_url('assets/js/jquery.counterup.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>-->
    <!-- JS jquery.waypoints-->
    <script src="<?php echo base_url('assets/js/jquery.waypoints.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>-->
    <!-- JS Chart2.js-->
    <script src="<?php echo base_url('assets/js/Chart2.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>-->
    <!-- JS Chart.bundle-->
    <script src="<?php echo base_url('assets/js/Chart.bundle.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>-->
<script src="<?php echo base_url('assets/vendors/charts/utils.js');?>"></script>
    <!-- JS jquery.knob-->
    <script src="<?php echo base_url('assets/js/jquery.knob.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>-->
    <!-- JS jquery.sparkline-->
    <script src="<?php echo base_url('assets/js/jquery.sparkline.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>-->
<script src="<?php echo base_url('assets/vendors/charts/excanvas.js');?>"></script>
    <!-- JS mithril-->
    <script src="<?php echo base_url('assets/js/mithril.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/mithril/1.1.1/mithril.js"></script>-->
<script src="<?php echo base_url('assets/vendors/theme-widgets/widgets.js');?>"></script>
    <!-- JS moment-->
    <script src="<?php echo base_url('assets/js/moment.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>-->
    <!-- JS underscore-->
    <script src="<?php echo base_url('assets/js/underscore.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>-->
    <!-- JS clndr-->
    <script src="<?php echo base_url('assets/js/clndr.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>-->
    <!-- JS morris5-->
    <script src="<?php echo base_url('assets/js/morris5.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>-->
    <!-- JS raphael-->
    <script src="<?php echo base_url('assets/js/raphael.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>-->
    <!-- JS daterangepicker-->
    <script src="<?php echo base_url('assets/js/daterangepicker.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>-->
    <!-- JS slick6-->
    <script src="<?php echo base_url('assets/js/slick6.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>-->
    <!-- JS dataTables15-->
    <script src="<?php echo base_url('assets/js/dataTables15.js');?>"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.15/js/jquery.dataTables.min.js"></script>-->
    <!-- JS sweetalert-->
    <script src="<?php echo base_url('assets/js/sweetalert2.js');?>"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>-->
<script src="<?php echo base_url('assets/plugins/bootstrap_datepicker/js/bootstrap-datepicker.js');?>"></script>
<script src="<?php echo base_url('assets/js/theme.js?'.uniqid());?>"></script>
<script src="<?php echo base_url('assets/js/eo.js');?>"></script>
<script src="<?php echo base_url('assets/js/submit.js');?>"></script>
<script src="<?php echo base_url('assets/js/custom.js?'.uniqid());?>"></script>
<script>
 $(document).ready(function() {
    $(".file_input_logo").hide(); 
      $(".datatable").DataTable();
       $(".date_picker").datepicker({
    format: 'yyyy-mm-dd',
  });
 });
    $(".change_picture").click(function(){
  var attr=$(this).attr('data-file');
  $("#"+attr).click();
});

</script>
</body>

</html>