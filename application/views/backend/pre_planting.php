           
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.css');?>' rel='stylesheet' />
<link href='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.print.min.css');?>' rel='stylesheet' media='print' />
<script src='<?php echo base_url('assets/plugins/fullcalendar/lib/moment.min.js');?>'></script>
<script src='<?php echo base_url('assets/plugins/fullcalendar/fullcalendar.min.js?1');?>'></script>
<!-- <script src='<?php //echo base_url('assets/js/event_calendar.js');?>'></script> -->
<link href="<?php echo base_url('assets/css/event_calendar.css');?>" rel="stylesheet">
<!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h5 class="mr-0 mr-r-5">Add Plant</h5>
                </div>
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <!-- Counters -->
                <!-- /.row -->
                <!-- Chart Group 1 -->
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-primary btn-md" href="<?php echo base_url('add_plant');?>"><span class="fas fa-arrow-left"></span> Back</a>
                    <h5>Plants Information : </h5>
                    </div>
                    <?php
                    if(!empty($plants)){
                    ?>
                        <div class="col-md-12 col-sm-12 widget-holder widget-full-height">
                        <div class="card plants-detail" >
                          <div class="card-body">
                          <img class="card-img-top" src="<?php echo base_url($plants->picture);?>" alt="Tomato">
                              <br>
                            <h5 class="card-title" style="text-align:center"><?php echo $plants->name.' (<i>'.$plants->latin_name.'</i>)';?></h5>
                              <label>Description : </label>
                              <p class="card-description">
                              <?php echo $plants->description;?>
                              </p>
                              <label>Uses : </label>
                              <p class="card-description">
                              <?php echo $plants->uses;?>
                              </p>
                              <label>Nutritions : </label>
                              <p class="card-description">
                              <?php echo $plants->nutritions;?>
                              </p>
                              <label>Harvest Time : </label>
                              <p class="card-description">
                              <?php echo $plants->harvest_time;?> Days
                              </p>
                              <label>Storage Devie : </label>
                              <p class="card-description">
                              <?php echo $plants->storage_device;?> Days
                              </p>
                              <label>Difficulty Level : </label>
                              <p class="card-description">
                              <?php echo strtoupper($plants->difficulty_level);?>
                              </p>
                          </div>
                        </div>
                           
                            </div>
                    <div class="col-md-12">
                        <a type="submit" class="btn btn-primary" href="<?php  echo base_url('select_mode/'.$plants->id);?>" ><span class="fas fa-arrow-right"></span> CONTINUE</a>
                    </div>
                    <?php } else echo 'Sorry, There\'s no plants to choose.';?>
                    <!-- /.widget-holder -->
                </div>
<!--
                	<div style="col-md-12 col-sm-12 widget-holder widget-full-height">
		<canvas id="canvas"></canvas>
	</div>
-->
	<br>
	<br>
                <!-- /.row -->
                <!-- Chart Group 2 -->
                <div class="row">
                    <!-- Charts: Tasks -->
                    <div class="col-md-6 col-sm-6 widget-holder widget-full-height">
                    </div>
                    <!-- /.widget-holder -->

            </div>
            <!-- /.widget-list -->
            </div>