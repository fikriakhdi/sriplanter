<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-newspaper-o"style="padding-right:10px"></span> Surat Jalan</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="sj_list_menu" href="#sj_list" class="active" data-toggle="tab">Surat Jalan List</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="sj_list">
      <div class="content-datatable table-responsive">
         <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr class="title-datable">
                <th>NO</th>
                <th>Nama Pelanggan</th>
                <th>Total Keseluruhan</th>
                <th>Tanggal Penjualan</th>
                <th>Cetak</th>
              </tr>
            </thead>
            <tbody>
                <?php
                $data_list = get_all_sj_list();
                if($data_list!=false){
                    $num=0;
                    foreach($data_list->result() as $data){
                        $num++;
                        ?>
              <tr>
                <td><?php echo $num;?></td>
                <td><?php echo $data->nama_pelanggan;?></td>
                <td><?php echo money($data->total_keseluruhan);?></td>
                <td><?php echo $data->tgl_penjualan;?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Cetak"><a href="<?php echo base_url('backend/cetak_sj/'.$data->id_penjualan);?>" class="btn btn-warning btn-xs" data-title="Print"  ><span class="fa fa-print"></span></a></p></td></tr>
                <?php }} ?>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
