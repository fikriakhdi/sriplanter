<section class="content list-content">
    <div class="row">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Redeem</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('user_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/redeem_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $data_edit->id_point;?>">
                            <div class="form-group">
                                <label for="nama_Penjualan">Pelanggan<span style="color:#f00">*</span></label>
                                <select name="id_pelanggan" id="id_pelanggan" class="form-control">
                                  <?php
                                  $pelanggan_list = get_all_pelanggan_list();
                                  if($pelanggan_list!=false){
                                    echo '<option>Pilih Pelanggan</option>';
                                    foreach($pelanggan_list->result() as $pelanggan){
                                      echo '<option value="'.$pelanggan->id_pelanggan.'">'.$pelanggan->nama_pelanggan.'</option>';
                                    }
                                  } else echo '<option>Tidak ada pilihan pelanggan</option>';
                                  ?>
                                </select>
                                <script>
                                $("#id_pelanggan").val('<?php echo $data_edit->id_pelanggan;?>').change();
                                </script>
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">Besar Point<span style="color:#f00">*</span></label>
                              <input type="number" class="form-control" id="alamat" name="total_point" aria-describedby="emailHelp" placeholder="Ketik Poin"   value="<?php echo $data_edit->total_point;?>" required>
                            </div>
                            <div class="footer-form">
                              <br>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>

        </div>
    </div>
    </div>
</section>
