<!--open content pro palanting-->
<div class="pos-title">
    <h3>DETAIL PROJECT</h3>
</div>
<div class="content-pro-planting" id="pro-palnting">
    <a class="btn btn-primary btn-md" href="<?php echo base_url('select_mode/'.$plants_id);?>"><span class="fas fa-arrow-left"></span> Back</a><br><br>
    <form class="" method="" action="" id="form-pro-planting">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Project Name" name="project_name_pro_plant" id="project_name_pro_plant">
        </div>
        <div class="form-group">
            <input type="text" class="form-control datepicker" placeholder="Date Start" name="start_date_pro_plant" id="start_date_pro_plant">
        </div>
        <div class="form-group">
            <input type="text" class="form-control datepicker" placeholder="Harvest Estimation" name="harverst_estimation_pro_plant" id="harverst_estimation_pro_plant">
        </div>
        <div class="pos-form-2-pro-planting">
            <div class="post-title-form">
                <h3 class="secon-title-form">Input Prameter</h3>
            </div>
            <div class="post-form">
                <div class="form-left">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Soil Moisture Min" name="soil_moisture_min" id="soil_moisture_min">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Light Min" name="light_min" id="light_min">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Humidity Min" name="humidity_min" id="humidity_min">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Temprature Min" name="temprature_min" id="temprature_min">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="pH Min" name="ph_min" id="ph_min">
                    </div>
                </div>
                <div class="form-right">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Soil Moisture Max" name="soil_moisture_max" id="soil_moisture_max">
                    </div><div class="form-group">
                        <input type="text" class="form-control" placeholder="Light Max" name="light_max" id="light_max">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Humidity Max" name="humidity_max" id="humidity_max">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Temprature Max" name="temprature_max" id="temprature_max">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="pH Max" name="ph_max" id="ph_max">
                    </div>
                </div>
            </div>
        </div>
            <div class="pos-add-more-device">
                <div class="title-secod">
                    <h3 class="secon-title-form">Syncronize Your Device</h3>
                </div>
                <div class="pos-col-add-device-more">
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more">
                            <img class="" src="assets/img/plants/plants-02.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more">
                            <img class="" src="assets/img/plants/plants-03.png">
                              <div class="overlay-start-planting">
                                <div class="text-start-palnting">
                                    <div class="pos-btn-01">
                                        <div class="left">
                                            <button type="button"><i class=" delete list-icon material-icons">close</i></button></div>
                                        <div class="right">
                                            <button type="button" data-toggle="modal" data-target="#AddDevice"><span class="fa fa-edit edit"></span></button>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-plus-add-device">+</a>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-plus-add-device">+</a>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-plus-add-device">+</a>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-plus-add-device">+</a>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-plus-add-device">+</a>
                        </div>
                    </div>
                    <div class="col-md-3 pos-cont-add-device-more">
                        <div class="pos-cont-col-add-device-more dashed-border">
                            <a href="#" class="btn-add-more-add-device">Add More Device</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pos-btn-submit-start-planting">
                <button stype="button" class="btn-orange btn-primary">save change</button>
            </div>
        </form>
</div>

<!--open modal input device id-->
  <div class="modal fade" id="AddDevice" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Device</h4>
        </div>
        <div class="modal-body">
          <form>
              <div class="form-group form-group-custom">
                  <input type="text" class="form-control form-control-custom " placeholder="Inut Device Code" id="device_id" name="device_id">
              </div>
          </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn-save-custom" data-dismiss="modal">SAVE DEVICE</button>
            <button type="button" class="btn-cancle-custom" data-dismiss="modal">CANCLE</button>
        </div>
      </div>
      
    </div>
  </div>
<!--close modal input device id-->