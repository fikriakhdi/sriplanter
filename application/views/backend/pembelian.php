<section class="content list-content">
    <div class="row">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-shopping-cart"style="padding-right:10px"></span> Pembelian</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="clearfix">
      <div class="tabbable-panel margin-tops4  datatble-content">
        <div class="tabbable-line">
          <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
         <li> <a id="pembelian_list_menu" href="#pembelian_list" class="active" data-toggle="tab">Pembelian List</a> </li>
         <li> <a id="add_pembelian_menu" href="#add_pembelian" data-toggle="tab"> Add Pembelian</a> </li>
          </ul>
          <div class="tab-content margin-tops">
          <!--Tab1-->
            <div class="tab-pane active fade in" id="pembelian_list">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Supplier</th>
              <th>Nama</th>
              <th>Jenis</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Kuantitas</th>
              <th>Total Pembelian</th>
              <th>Tanggal Pembelian</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $data_list = get_all_pembelian_list();
              if($data_list!=false){
                  $num=0;
                  foreach($data_list->result() as $data){
                      $num++;
                      ?>
            <tr>
              <td><?php echo $num;?></td>
              <td><?php echo $data->nama_supplier;?></td>
              <td><?php echo $data->nama_produk;?></td>
              <td><?php echo $data->jenis_produk;?></td>
              <td><?php echo $data->satuan_produk;?></td>
              <td><?php echo money($data->harga_produk);?></td>
              <td><?php echo $data->kuantitas;?></td>
              <td><?php echo money($data->total_pembelian);?></td>
              <td><?php echo $data->tanggal_pembelian;?></td>
              <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_pembelian/'.$data->id_pembelian);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="tab-pane fade in" id="add_pembelian">
      <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_pembelian');?>" enctype="multipart/form-data">
        <input name="id" type="hidden" value="">
        <div class="form-group">
          <label>Supplier<span style="color:#f00">*</span></label>
          <select class="form-control" name="id_supplier" required>
            <?php
            $supplier_list = get_all_supplier_list();
            if($supplier_list!=false){
              echo '<option>Pilih Supplier</option>';
              foreach($supplier_list->result() as $supplier){
                echo '<option value="'.$supplier->id_supplier.'">'.$supplier->nama_supplier.'</option>';
              }
            } else echo '<option>Maaf tidak ada pilihan supplier</option>';
            ?>
          </select>
        </div>
        <div class="form-group">
          <label>Produk<span style="color:#f00">*</span></label>
          <select class="form-control" name="id_pembelian_produk" required>
            <?php
            $data_list = get_all_pembelian_produk_list();
            if($data_list!=false){
              echo '<option>Pilih Produk</option>';
              foreach($data_list->result() as $data){
                echo '<option value="'.$data->id_pembelian_produk.'">'.$data->nama_produk.' ('.money($data->harga_produk).')</option>';
              }
            } else echo '<option>Maaf tidak ada pilihan produk</option>';
            ?>
          </select>
        </div>
        <div class="form-group">
          <label>Kuantitas<span style="color:#f00">*</span></label>
          <input type="number" class="form-control" name="kuantitas" placeholder="Ketik kuantitas"  value="" required>
        </div>
        <div class="form-group">
          <label>Tanggal Pembelian<span style="color:#f00">*</span></label>
          <input type="text" class="form-control datepicker" name="tanggal_pembelian" placeholder="Ketik Tanggal Pembelian" autocomplete="off" value="" required>
        </div>
          <div class="footer-form">
            <br>
              <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
    </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>
