<!--open content view all chart-->
<div class="pos-title">
    <h3>ALL REPORT - TOMAT 1</h3>
</div>
<div class="content-view-all-chart" id="view_all_chart">
   <div class="pos-head-button-chart">
        <div class="left">
            <div class="form-group">
                <select name="carlist" form="carform">
                  <option value="Weekly1">Weekly 1</option>
                  <option value="Weekly2">Weekly 2</option>
                  <option value="Weekly3">Weekly 3</option>
                  <option value="Weekly4">Weekly 4</option>
                <option value="Weekly5">Weekly 5</option>
                </select>
            </div>
        </div>
        <div class="right">
            <button class="btn-blue-custom" onclick="">EXPORT TO CSV</button>
        </div>
    </div>
    <div class="pos-all-chart">
        <div class="left">
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Water Usage</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="water_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Soil PH</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="soil_ph_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Temprature</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="temprature_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Humidity</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="humidity_chart"></div>
                </div>
            </div>
        </div>
        <div class="right">
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Soil Moisture</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="soil_moisture_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Light</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="light_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Farming Days</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="farming_days_chart"></div>
                </div>
            </div>
            <div class="content-chart-all">
                <div class="title-chart">
                    <h3>Harvest Estimation</h3>
                </div>
                <div class="pos-chart-all-report">
                    <div class="content-chart" id="harvest_estimation_chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--open content view all chart-->