<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        404 Error
    </title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo ASSETS;?>img/Favicon-02.png"/>
    <!--fixed header data table-->
    <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <!--bootstrap respopnsive-->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?php echo ASSETS;?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Datatables -->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="<?php echo ASSETS;?>vendors/animate/animate.css" rel="stylesheet">
    <!-- Icon CSS-->
    <link rel="stylesheet" href="<?php echo ASSETS;?>vendors/font-awesome/css/font-awesome.min.css">
    <!-- Camera Slider -->
    <link rel="stylesheet" href="<?php echo ASSETS;?>vendors/camera-slider/camera.css">
    <!-- Owlcarousel CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>vendors/owl_carousel/owl.carousel.css" media="all">

    <!--Theme Styles CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS;?>css/style.css" media="all" />
    <!-- Electra Custom Style -->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/electra_style.css?<?php echo rand();?>">

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo ASSETS;?>js/html5shiv.min.js"></script>
    <script src="<?php echo ASSETS;?>js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Content Header (Page header) -->
    <div class="container container-error">
        <div class="site-logo clearfix">
            <a href="#">
                <h1><?php echo COMPANY_NAME;?>
            </a>
        </div>
        <div class="col clearfix">
            <div class="col--m9">
                <h1 class="error-title">
                    <span class="icon-report-problem"></span> Ooopss
                    <span>Halaman tidak tersedia</span>
                </h1>
                <a class="btn btn-primary" href="<?php echo BASE_URL;?>" style="color:white">Kembali ke Halaman Depan</a>
            </div>
        </div>
    </div>
    <!-- /.content -->
</body>
</html>
