<?php
class article_model extends CI_Model{

  var $article_model                  = 'article';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_article_model($data){
        $this->db->insert($this->article_model,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_article_model($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->article_model);
        $query=$this->db->get();
        return $query;
    }
    function update_article_model($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->article_model,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_article_model($id){
        $this->db->where('id',$id);
        $this->db->delete($this->article_model);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>