<?php
class plant_device_model extends CI_Model{
  var $plant_device                     = 'plant_device';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_plant_device($data){
        $this->db->insert($this->plant_device,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_plant_device($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->plant_device);
        $query=$this->db->get();
        return $query;
    }
    function update_plant_device($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->plant_device,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_plant_device($id){
        $this->db->where('id',$id);
        $this->db->delete($this->plant_device);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
