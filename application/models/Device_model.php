<?php
class device_model extends CI_Model{
  var $device                     = 'device';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_device($data){
        $this->db->insert($this->device,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_device($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->device);
        $query=$this->db->get();
        return $query;
    }
    function update_device($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->device,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_device($id){
        $this->db->where('id',$id);
        $this->db->delete($this->device);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
