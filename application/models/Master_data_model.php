<?php
class master_data_model extends CI_Model{
  var $master_data                     = 'master_data';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_master_data($data){
        $this->db->insert($this->master_data,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_master_data($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->master_data);
        $query=$this->db->get();
        return $query;
    }
    function update_master_data($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->master_data,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_master_data($id){
        $this->db->where('id',$id);
        $this->db->delete($this->master_data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
