<?php
class device_owner_model extends CI_Model{
  var $device_owner                     = 'device_owner';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_device_owner($data){
        $this->db->insert($this->device_owner,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_device_owner($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->device_owner);
        $query=$this->db->get();
        return $query;
    }
    function update_device_owner($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->device_owner,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_device_owner($id){
        $this->db->where('id',$id);
        $this->db->delete($this->device_owner);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
