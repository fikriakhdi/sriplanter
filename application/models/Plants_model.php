<?php
class plants_model extends CI_Model{
  var $plants                     = 'plants';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_plants($data){
        $this->db->insert($this->plants,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_plants($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->plants);
        $query=$this->db->get();
        return $query;
    }
    function update_plants($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->plants,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_plants($id){
        $this->db->where('id',$id);
        $this->db->delete($this->plants);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
