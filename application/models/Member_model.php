<?php
class member_model extends CI_Model{
  var $member                     = 'member';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_member($data){
        $this->db->insert($this->member,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_member($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->member);
        $query=$this->db->get();
        return $query;
    }
    function update_member($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->member,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_member($id){
        $this->db->where('id',$id);
        $this->db->delete($this->member);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
