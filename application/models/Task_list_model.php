<?php
class task_list_model extends CI_Model{
  var $task_list                     = 'task_list';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_task_list($data){
        $this->db->insert($this->task_list,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_task_list($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->task_list);
        $query=$this->db->get();
        return $query;
    }
    function update_task_list($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->task_list,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_task_list($id){
        $this->db->where('id',$id);
        $this->db->delete($this->task_list);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
