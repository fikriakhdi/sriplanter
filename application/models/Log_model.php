<?php
class log_model extends CI_Model{
  var $log                     = 'log';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_log($data){
        $this->db->insert($this->log,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_log($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->log);
        $query=$this->db->get();
        return $query;
    }
    function update_log($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->log,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_log($id){
        $this->db->where('id',$id);
        $this->db->delete($this->log);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
