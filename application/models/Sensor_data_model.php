<?php
class sensor_data_model extends CI_Model{
  var $sensor_data                     = 'sensor_data';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_sensor_data($data){
        $this->db->insert($this->sensor_data,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_sensor_data($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->sensor_data);
        $query=$this->db->get();
        return $query;
    }
    function update_sensor_data($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->sensor_data,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_sensor_data($id){
        $this->db->where('id',$id);
        $this->db->delete($this->sensor_data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
