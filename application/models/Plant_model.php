<?php
class plant_model extends CI_Model{
  var $plant                     = 'plant';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_plant($data){
        $this->db->insert($this->plant,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_plant($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->plant);
        $query=$this->db->get();
        return $query;
    }
    function update_plant($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->plant,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_plant($id){
        $this->db->where('id',$id);
        $this->db->delete($this->plant);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
