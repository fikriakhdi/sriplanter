<?php
class otp_model extends CI_Model{
  var $otp                     = 'otp';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_otp($data){
        $this->db->insert($this->otp,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_otp($where="", $limit="", $order_by=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->otp);
        $query=$this->db->get();
        return $query;
    }
    function update_otp($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->otp,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_otp($id){
        $this->db->where('id',$id);
        $this->db->delete($this->otp);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
