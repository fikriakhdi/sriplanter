<?php
class water_usage_model extends CI_Model{
  var $water_usage                     = 'water_usage';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_water_usage($data){
        $this->db->insert($this->water_usage,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_water_usage($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->water_usage);
        $query=$this->db->get();
        return $query;
    }
    function update_water_usage($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->water_usage,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_water_usage($id){
        $this->db->where('id',$id);
        $this->db->delete($this->water_usage);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
