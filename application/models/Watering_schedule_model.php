<?php
class watering_schedule_model extends CI_Model{

  var $watering_schedule                  = 'watering_schedule';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_watering_schedule($data){
        $this->db->insert($this->watering_schedule,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_watering_schedule($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->watering_schedule);
        $query=$this->db->get();
        return $query;
    }
    function update_watering_schedule($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->watering_schedule,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_watering_schedule($id){
        $this->db->where('id',$id);
        $this->db->delete($this->watering_schedule);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
