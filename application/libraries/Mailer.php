<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Email class.
 *
 * @class Mailer
 * @author Fikri
 */
 class Mailer
 {
 	var $CI;
     var $active;

 	/**
 	 * Constructor - Sets up the object properties.
 	 */
 	function __construct()
     {
         $this->CI       =& get_instance();
         $this->active	= TRUE;
         $config['protocol'] = "smtp";
         $config['smtp_host'] = 'smtp.gmail.com';
         $config['smtp_port']    = '587';
         $config['smtp_timeout'] = '7';
         $config['smtp_crypto'] = 'tls';
         $config['smtp_user']    = 'maningcorp@gmail.com';
         $config['smtp_pass']    = 'maning123!';
         $config['charset']    = 'iso-8859-1';
         $config['newline']    = "\r\n";
         $config['mailtype'] = 'html'; // or html
         $config['validation'] = TRUE; // bool whether to validate email or not
         $this->CI->load->library('email', $config);
         $this->CI->email->set_newline("\r\n");
 	}

     /**
 	 * Send email function.
 	 *
      * @param string    $to         (Required)  To email destination
      * @param string    $subject    (Required)  Subject of email
      * @param string    $message    (Required)  Message of email
      * @param string    $from       (Optional)  From email
      * @param string    $from_name  (Optional)  From name email
 	 * @return Mixed
 	 */
 	function send($to, $subject, $message, $from = '', $from_name = ''){
 		$this->CI->email->to($to);
         $this->CI->email->from($from, $from_name);
         $this->CI->email->subject($subject);
         $this->CI->email->message($message);

         return $this->CI->email->send();
 	}

     function send_email_verification($email,$password, $verification_url){

         $html_down         = get_settings('send_email_verification_html');
         $html_down         = str_replace("%email%",             $email, $html_down);
         $html_down         = str_replace("%password%",             $password, $html_down);
         $html_down         = str_replace("%website_url%",          base_url(), $html_down);
         $html_down         = str_replace("%verification_url%",            $verification_url, $html_down);
         $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
         $html_down         = str_replace("%year%",     			date('Y'), $html_down);

         $send               = $this->send($email, 'Account Verification', $html_down, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

             return $send;
     }

     function send_email_admin_create_member($email,$password){
         $email             = trim($email);

         $html_down         = get_settings('send_email_admin_create_member_html');
         $html_down         = str_replace("%email%",             $email, $html_down);
         $html_down         = str_replace("%password%",             $password, $html_down);
         $html_down         = str_replace("%website_url%",          base_url(), $html_down);
         $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
         $html_down         = str_replace("%year%",     			date('Y'), $html_down);

         $send               = $this->send($email, 'Notifikasi Pendaftaran', $html_down, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

             return $send;
     }

     function send_email_change_password($email,$password){

         $html_down      = get_settings('send_email_change_password_html');
         $admin_contact  = get_settings('admin_contact');


         $html_down         = str_replace("%email%",             $email, $html_down);
         $html_down         = str_replace("%password%",             $password, $html_down);
         $html_down         = str_replace("%website_url%",          base_url(), $html_down);
         $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
         $html_down         = str_replace("%year%",     			date('Y'), $html_down);
         $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);

         $message            = $html_down;
         $send               = $this->send($email, 'Informasi Ubah Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

             return $send;
     }

     function send_email_reset_password($email,$password){

         $html_down      = get_settings('send_email_forgot_html');
         $admin_contact  = get_settings('admin_contact');

         $html_down         = str_replace("%email%",             $email, $html_down);
         $html_down         = str_replace("%password%",             $password, $html_down);
         $html_down         = str_replace("%website_url%",          base_url(), $html_down);
         $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
         $html_down         = str_replace("%year%",     			date('Y'), $html_down);
         $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);

         $message            = $html_down;

         $send               = $this->send($email, 'Informasi Reset Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

             return $send;
     }
 }
