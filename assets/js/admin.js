function imagepreview(input,image_id)
{
  if (input.files && input.files[0])
  {
    var filerdr = new FileReader();
    filerdr.onload = function(e) {
        $('#'+image_id).attr('src', e.target.result);
    };
    filerdr.readAsDataURL(input.files[0]);
  }
}
$(".change_picture").click(function(){
  var attr=$(this).attr('data-file');
  $("#"+attr).click();
});
$(".change_picture").hover(function() {
    $(this).css('cursor','pointer');
});

$(document).ready(function() {
  var url = $(location).attr('href'),
      parts = url.split("/"),
      last_part = parts[parts.length-1];
      if(last_part!=""){
      $("#"+last_part).addClass("active");
    } else {
      $("#dashboard").addClass("active");
    }
  $(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
  });
  $(".datatable").DataTable();
    $(".file_input_logo").hide();
  $(".table").DataTable();
    $('.tokenfield').tokenfield({
       delimiter:';'
    });
});

$(document).ready(function() {
        $('.summernote').summernote({
            minHeight: 200,
             callbacks: {
                        onImageUpload : function(files, editor, welEditable) {

                             for(var i = files.length - 1; i >= 0; i--) {
                                     sendFile(files[i], this);
                            }
                        }
                    }
        });
    function sendFile(file, el) {
    var form_data = new FormData();
    form_data.append('file', file);
    $.ajax({
        data: form_data,
        type: "POST",
        url: 'http://a3.maningcorp.com/backend/upload_file_summernote',
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            $(el).summernote('editor.insertImage', url);
        }
    });
    }
    });

$(".delete_btn").click(function(e){
    e.preventDefault();
    var url = $(this).attr("id_url");
    $("#delete_footer").attr("href", url);
});

$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        },
         function () {
           $('.image-preview').popover('hide');
        }
    );
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'top'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });
});


$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});

function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
