$(".delete-confirm").click(function(e){
  e.preventDefault();
  var url=$(this).attr('data-url');

  $("#fade_modal").show();
  $("#delete_id").attr("href", url);
});
$(".close-modal").click(function(e){
    e.preventDefault();
    $("#fade_modal").hide();
});
$(window).scroll(function() {

    //After scrolling 100px from the top...
    if ( $(window).scrollTop() >= 100 ) {

        $('.navbar').removeClass('transparent_red');
        $('.navbar').addClass('background-color');

    //Otherwise remove inline styles and thereby revert to original stying
    } else {
        var container = $("#container_body");
        if(container.attr("color")=="transparent"){
        $('.navbar').addClass('transparent_red');
        $('.navbar').removeClass('background-color');
        }
    }
}); 

$(document).ready(function(){
    var container = $("#container_body");
        if(container.attr("color")=="transparent"){
        $('.navbar').addClass('transparent_red');
        $('.navbar').removeClass('background-color');
        }

    $(".datatable").DataTable();
});

/**open js for navbar mobile **/
$(document).ready(function() {
    $("#fade_modal").hide();
  $('#show-hidden-menufm').click(function() {
    $('.hidden-menufm').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

/**open js for navbar mobile **/
$(document).ready(function() {
  $('#show-hidden-menufm2').click(function() {
    $('.hidden-menufm2').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
function myFunction() {
    var x = document.getElementById("mySidenav");
    if (x.style.width === "0") {
        x.style.width = "100%";
    } else {
        x.style.width = "0";
    }
}
// $('#menu-toggle').click(function(){
//     $(this).toggleClass('open');
//  })
var percent = 0;
var direction = "down";
window.setInterval(function(){
  if(direction=="down" && percent>=0 && percent<100)
  percent+=1;
  if(percent==100) direction="up";
  if(percent==0) direction="down";
  if(direction=="up" && percent>0 && percent<=100)
  percent-=1;
  $(".screen-content-scroll").animate({"background-position-y":percent+"%"},100);
}, 125);

$("#toggle_menusv2").click(function() {
  $(this).toggleClass("on");
  $("#menu_v2").slideToggle();
});

// $(document).ready(function() {
//   $('#show-hidden-menu').click(function() {
//     $('.hidden-menu').slideToggle("slow");
//     // Alternative animation for example
//     // slideToggle("fast");
//   });
// });
/**close js for navbar mobile **/




/**button scroll to top**/

$(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  }); // click() scroll top EMD

  /*************************************
    LEFT MENU SMOOTH SCROLL ANIMATION
   *************************************/
  // declare variable
  var h1 = $("#h1").position();
  var h2 = $("#h2").position();
  var h3 = $("#h3").position();

  $('.link1').click(function() {
    $('html, body').animate({
      scrollTop: h1.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link2').click(function() {
    $('html, body').animate({
      scrollTop: h2.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link3').click(function() {
    $('html, body').animate({
      scrollTop: h3.top
    }, 500);
    return false;

  }); // left menu link3 click() scroll END

}); // ready() END

/**-------------button share animation------------------**/
$(document).ready(function() {
    $('.date_picker').datepicker({
    format: 'yyyy-mm-dd'
});
    $(".table").DataTable();
    $(".file_input_logo").hide(); 
  $('#show-hidden-menu').click(function() {
    $('.hidden-menu').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

/**------------smooth scroll to id--------------**/
$('.scrollTo').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});

/**-------------js for rotated span menu dropdown mobile------------**/
$(".rotate").click(function () {
    $(this).toggleClass("down");
});

/**-------------js for scroll indicator------------------**/

/**-------------js for share button modal popup------------------**/

 $('.copy-btn').on("click", function(){
        value = $(this).data('clipboard-text'); //Upto this I am getting value

        var $temp = $("<input>");
          $("body").append($temp);
          $temp.val(value).select();
          document.execCommand("copy");
          $temp.remove();
     alert("Link Copied");
    });
function imagepreview(input,image_id)
{
  if (input.files && input.files[0])
  {
    var filerdr = new FileReader();
    filerdr.onload = function(e) {
        $('#'+image_id).attr('src', e.target.result);
    };
    filerdr.readAsDataURL(input.files[0]);
  }
}
$(".change_picture").click(function(){
  var attr=$(this).attr('data-file');
  $("#"+attr).click();
});
$(".show-element").click(function(e){
 e.preventDefault();
var target = $(this).data('target');
var status = $(this).data('status');
//if(status=='show'){
$("#sri_planting").hide();
$("#pro_planting").hide();
$(target).show();
//    $(".active-element").hide();
//    $(target).removeClass('active-element');
//    $(target).hide();
//}
//if(status=='hide'){
//   $("#sri_planting").hide();
//    $("#pro_planting").hide();
//    $(target).show();
//}
})

$(".div-link").click(function(e){
  e.preventDefault();
    var url=$(this).attr("href");
    window.location.href=url;
});

$(".harvest_estimation").change(function(e){
   e.preventDefault(); 
    var submit_url   = $(this).attr('url');
    var date_start   = $(this).val();
    var harvest_time = $(this).attr('harvest-time');
    $.ajax({
    type: "POST",
    url: submit_url,
    data: {
        date_start:date_start,
        harvest_time : harvest_time
    },
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.status=="200"){
          $("#harverst_estimation").val(result.data);
      }
      else if(result.info!="200"){
        $("#harverst_estimation").val('Invalid Date');
      }
    },
    dataType: "json"
  });
});

$(".add-more-device").click(function(e){
    e.preventDefault();
    var slot = $("#total_slot").val();
    var new_slot = parseInt(slot)+1;
    $("#total_slot").val(new_slot);
    var pic_url = $("#picture_url").val();
    var element = '<div class="col-md-3 pos-cont-add-device-more"><div class="pos-cont-col-add-device-more hidden" id="added_device_'+new_slot+'"><img class="image" src="'+pic_url+'"><div class="overlay-start-planting"><div class="text-start-palnting"><div class="pos-btn-01"><div class="left"><button type="button"><i class=" delete list-icon material-icons delete-button" data-sequence="'+new_slot+'" >close</i></button></div><div class="right"><button type="button" data-toggle="modal" data-target="#AddDevice" class="edit-device"><span class="fa fa-edit edit"  data-target="#EditDevice" data-sequence="'+new_slot+'"></span></button></div></div></div></div></div><div class="pos-cont-col-add-device-more dashed-border" id="empty_device_'+new_slot+'"><button type="button" class="btn-plus-add-device save-device" data-sequence="'+new_slot+'" data-toggle="modal" data-target="#AddDevice">+</button></div><input type="hidden" name="devices[]" id="value_device_'+new_slot+'"></div>';
    $("#synchronize_device").append(element);
    swal("Your Device slot is successfully added!", {
              icon: "success",
    });
});
$(document).on('click', '.save-device', function(e) {
   var sequence = $(this).data('sequence'); 
    $("#sequence_data").val(sequence);
});

$(document).on('click', '.edit-device', function(e) {
   var sequence = $(this).data('sequence'); 
    $("#sequence_data").val(sequence);
    $("#device_id_edit").val($("#value_device_"+sequence).val());
    $("#sequence_data_edit").val(sequence);
});

$(document).on('click', '.edit-device-id', function(e) {
    e.preventDefault();
     var submit_url = $(this).data('url');
    var device_code = $("#device_id_edit").val();
    //check code
    $.ajax({
    type: "POST",
    url: submit_url,
    data: {
        device_code:device_code
    },
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.status=="200"){
          //checking input devices
          var count = 0;
         $('.devices').each(function(){
              if($(this).val()==$("#device_id_edit").val()) count++;
            });
          if(count==1){
              var sequence = $("#sequence_data_edit").val();
                $("#value_device_"+sequence).val(device_code);
                $("#device_id").val("");
                swal("Your Device is successfully edited!", {
                          icon: "success",
                        });
                $("#sequence_data").val('');
                $("#cancel_edit_modal_device").click();
          } else {
                swal("Device code is already in the list", {
                          icon: "error",
                        });
          }
         
      }
      else if(result.info!="200"){
        swal(result.message, {
                          icon: "error",
                        });
      }
    },
    dataType: "json" 
  });
    
//   var sequence = $("#sequence_data_edit").val(); 
//    $("#sequence_data").val(sequence);
//    $("#value_device_"+sequence).val($("#device_id_edit").val());
//    swal("Your Device is successfully edited!", {
//              icon: "success",
//            });
//    $("#sequence_data").val('');
});
$(document).on('click', '.save-device-id',function(e){
    e.preventDefault();
    var submit_url = $(this).data('url');
    var device_code = $("#device_id").val();
    //check code
    $.ajax({
    type: "POST",
    url: submit_url,
    data: {
        device_code:device_code
    },
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.status=="200"){
          //checking input devices
          var count = 0;
          $('.devices').each(function(){
              if($(this).val()==$("#device_id").val()) count++;
            });
          if(count==0){
              var sequence = $("#sequence_data").val();
                $("#value_device_"+sequence).val(device_code);
                $("#empty_device_"+sequence).hide();
                $("#added_device_"+sequence).show();
                $("#device_id").val("");
                swal("Your Device is successfully added!", {
                          icon: "success",
                        });
                $("#sequence_data").val('');
                $("#cancel_modal_device").click();
                $("#AddDevice").modal('close');
          } else {
               swal("Device code is already in the list", {
                          icon: "error",
                        });
          }
      }
      else if(result.status!="200"){
        swal(result.message, {
                          icon: "error",
                        });
      }
    },
    dataType: "json"
  });

});

$(document).on('click', '.delete-button', function(e) {
    e.preventDefault();
    var sequence = $(this).data("sequence");
    swal({
          title: "Are you sure ??",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) {
            $("#added_device_"+sequence).hide();
            $("#value_device_"+sequence).hide();
            $("#value_device_"+sequence).val('');
            $("#empty_device_"+sequence).show();
            $("#added_device_"+sequence).hide(); 
            $("#sequence_data").val('');
            swal("Your Device is successfully deleted!", {
              icon: "success",
            });
          } else {
          }
        });
    
});

$("#submit-add-team").click(function(e) {
    e.preventDefault();
    $.ajax({
        url : $("#create-pro-team").attr("action"), 
           type: "post", //form method
           data: $("#create-pro-team").serialize(),
           dataType:"json", //misal kita ingin format datanya brupa json
           beforeSend:function(){
             //lakukan apasaja sambil menunggu proses selesai disini
             //misal tampilkan loading
             
             $(".loading").html("Please wait....");
             
           },
        success: function(){
            alert('terkirim');
        }
    });
  
  });