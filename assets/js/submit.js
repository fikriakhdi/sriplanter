$(".submit-btn").click(function(e){
  e.preventDefault();
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var submit_url        = form.attr('action');
  var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        $("#warning_message").html(result.message);
          if(refresh_url!="") window.location=refresh_url;
      }
      else if(result.info=="failed"){
        $("#warning_message").html(result.message);
      }
    },
    dataType: "json"
  });
});